//
//  WorkStatus.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

enum WorkStatus {
    
    case PAYMENT_FAIL
    case FIND_TRUCK
    case WAITING_CONFIRM_WORK
    case WAITING_RECEIVE_ITEM
    case ARRIVED_SOURCE
    case WAITING_SENT_ITEM
    case ARRIVED_DESTINATION
    case COMPLETE
    case CANCEL
    
    var status: Int {
        switch self {
        case .PAYMENT_FAIL:
            return 0
        case .FIND_TRUCK:
            return 1
        case .WAITING_CONFIRM_WORK:
            return 2
        case .WAITING_RECEIVE_ITEM:
            return 3
        case .ARRIVED_SOURCE:
            return 4
        case .WAITING_SENT_ITEM:
            return 5
        case .ARRIVED_DESTINATION:
            return 6
        case .COMPLETE:
            return 7
        case .CANCEL:
            return 8
        }
    }
    
    var readableStatus: String {
        switch self {
        case .PAYMENT_FAIL:
            return "จ่ายเงินด้วยบัตรไม่สำเร็จ"
        case .FIND_TRUCK:
            return "กำลังหารถ"
        case .WAITING_CONFIRM_WORK:
            return "รอคอนเฟิร์ม"
        case .WAITING_RECEIVE_ITEM:
            return "รอขึ้นของ"
        case .ARRIVED_SOURCE:
            return "ถึงต้นทาง"
        case .WAITING_SENT_ITEM:
            return "กำลังจัดส่ง"
        case .ARRIVED_DESTINATION:
            return "ถึงปลายทาง"
        case .COMPLETE:
            return "เสร็จงาน"
        case .CANCEL:
            return "งานยกเลิก"
        }
    }
    
    var readableAction: String {
        switch self {
        case .FIND_TRUCK:
            return "รับงาน"
        case .WAITING_CONFIRM_WORK:
            return "ยกเลิก"
        case .WAITING_RECEIVE_ITEM:
            return "ถึงต้นทาง"
        case .ARRIVED_SOURCE:
            return "ขึ้นของเสร็จ"
        case .WAITING_SENT_ITEM:
            return "ถึงปลายทาง"
        case .ARRIVED_DESTINATION:
            return "ลงของเสร็จ"
        default:
            return ""
        }
    }
    
    var StatusColor: UIColor {
        switch self {
        case .FIND_TRUCK:
            return UIColor(named: "Find Truck Color")!
        case .WAITING_CONFIRM_WORK:
            return UIColor(named: "Waiting Confirm Color")!
        case .WAITING_RECEIVE_ITEM:
            return UIColor(named: "Waiting Receive Item Color")!
        case .ARRIVED_SOURCE:
            return UIColor(named: "Arrived Source Color")!
        case .WAITING_SENT_ITEM:
            return UIColor(named: "Waiting Send Item Color")!
        case .ARRIVED_DESTINATION:
            return UIColor(named: "Arrived Destination Color")!
        case .COMPLETE:
            return UIColor(named: "Finish Color")!
        case .CANCEL:
            return UIColor(named: "Cancel Color")!
        default:
            return UIColor.clear
        }
    }
    
}

