//
//  TransactionRequestStatus.swift
//  OKTrucker
//
//  Created by amberhello on 25/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum TransactionRequestStatus {
    
    case WAITING
    case APPROVED
    case REJECT
    
    var status: Int {
        switch self {
        case .WAITING:
            return 0
        case .APPROVED:
            return 1
        case .REJECT:
            return 2
        }
    }
    
    var depositMessage: String {
        switch self {
        case .WAITING:
            return "รอเติม"
        case .APPROVED:
            return "เติมสำเร็จ"
        case .REJECT:
            return "ไม่อนุมัติ"
        }
    }
    
    var withdrawMessage: String {
        switch self {
        case .WAITING:
            return "รอโอน"
        case .APPROVED:
            return "โอนสำเร็จ"
        case .REJECT:
            return "ไม่อนุมัติ"
        }
    }
}
