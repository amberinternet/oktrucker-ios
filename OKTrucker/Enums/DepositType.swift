//
//  DepositType.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum DepositType {
    
    case OMISE
    case TRANSFER
    
    var value: Int {
        switch self {
        case .OMISE:
            return 1
        case .TRANSFER:
            return 2
        }
    }
    
    var method: String {
        switch self {
        case .OMISE:
            return "บัตรเครดิต/เดบิต"
        case .TRANSFER:
            return "เงินโอนแนบสลิป"
        }
    }
    
}
