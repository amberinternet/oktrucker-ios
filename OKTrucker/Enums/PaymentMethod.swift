//
//  PaymentMethod.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum PaymentMethod {
    
    case CASH_SOURCE
    case CASH_DESTINATION
    case WALLET
    case OMISE
    
    var status: Int {
        switch self {
        case .CASH_SOURCE:
            return 1
        case .CASH_DESTINATION:
            return 2
        case .WALLET:
            return 3
        case .OMISE:
            return 4
        }
    }
    
    var pay: String {
        switch self {
        case .CASH_SOURCE:
            return "ชำระเงินสดต้นทาง"
        case .CASH_DESTINATION:
            return "ชำระเงินสดปลายทาง"
        case .WALLET:
            return "ชำระด้วย Wallet"
        case .OMISE:
            return "ชำระด้วยบัตร"
        }
    }
    
    var receive: String {
        switch self {
        case .CASH_SOURCE:
            return "งานเงินสด (ชำระต้นทาง)"
        case .CASH_DESTINATION:
            return "งานเงินสด (ชำระปลายทาง)"
        case .WALLET:
            return "งานเครดิต"
        case .OMISE:
            return "งานเครดิต"
        }
    }
    
}

