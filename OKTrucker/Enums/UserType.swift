//
//  UserType.swift
//  OKTrucker
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

enum UserType {
    
    case DRIVER
    
    var type: String {
        switch self {
        case .DRIVER:
            return "driver"
        }
    }
    
}
