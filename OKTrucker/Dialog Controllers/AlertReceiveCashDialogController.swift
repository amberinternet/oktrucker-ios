//
//  AlertReceiveCashDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class AlertReceiveCashDialogController: UIViewController {

    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var paymentMethod: String? = nil
    var price: String? = nil
    var completion: () -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentMethodLabel.text = paymentMethod
        priceLabel.text = price
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func agree(_ sender: Any) {
        self.dismiss(animated: false, completion: completion)
    }

}
