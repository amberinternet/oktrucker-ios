//
//  ErrorDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ErrorDialogController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    var message: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        messageLabel.text = message
    }
    

    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

}
