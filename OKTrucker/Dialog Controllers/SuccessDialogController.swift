//
//  SuccessDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class SuccessDialogController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    var message: String? = nil
    var completion: () -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if title != nil { titleLabel.text = title }
        messageLabel.text = message
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: completion)
    }

}
