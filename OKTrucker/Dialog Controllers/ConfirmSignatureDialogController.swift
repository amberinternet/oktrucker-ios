//
//  ConfirmSignatureDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import SwiftSignatureView

class ConfirmSignatureDialogController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var signatureView: SwiftSignatureView!
    var completion: (_ signatureImage: UIImage) -> Void = {_ in }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if title != nil { titleLabel.text = title }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func clear(_ sender: Any) {
        signatureView.clear()
    }
    
    @IBAction func confirm(_ sender: Any) {
        if signatureView.signature != nil {
            self.dismiss(animated: false, completion: {self.completion(self.signatureView.signature!)})
        } else {
            DialogManager.default().showToast(message: NSLocalizedString("กรุณาเซ็นชื่อ", comment: ""))
        }
    }
    
}
