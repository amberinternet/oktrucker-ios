//
//  ConfirmAcceptWorkDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ConfirmAcceptWorkDialogController: UIViewController {

    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentMethodIcon: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var finalWageLabel: UILabel!
    var work: Work! = nil
    var completion: () -> Void = {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        paymentMethodLabel.text = work.getReadableReceiveMethod()
        priceLabel.text = work.getPrice()
        feeLabel.text = work.getFee()
        finalWageLabel.text = work.getFinalWage()
        
        switch work.paymentMethod {
        case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
            paymentMethodIcon.image = UIImage(named: "Dollar Note Icon")
        default:
            paymentMethodIcon.image = UIImage(named: "Wallet Icon")
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func agree(_ sender: Any) {
        self.dismiss(animated: false, completion: completion)
    }

}
