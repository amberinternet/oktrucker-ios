//
//  ContactAdminDialogController.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ContactAdminDialogController: UIViewController {
    
    @IBOutlet weak var adminPhoneNumberLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        adminPhoneNumberLabel.text = Constants.ADDMIN_PHONE_NUMBER
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        
        
    }
    
    @IBAction func callAdmin(_ sender: Any) {
        guard let number = URL(string: "tel://" + Constants.ADDMIN_PHONE_NUMBER) else { return }
        UIApplication.shared.open(number)
        self.dismiss(animated: false, completion: nil)
    }
}
