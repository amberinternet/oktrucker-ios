//
//  ContactView.swift
//  OKTrucker
//
//  Created by amberhello on 13/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ContactView: UIView {

    @IBOutlet weak var conpanyNameLabel: UILabel!
    @IBOutlet weak var landmarkLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!

}
