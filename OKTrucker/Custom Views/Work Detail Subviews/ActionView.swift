//
//  ActionView.swift
//  OKTrucker
//
//  Created by amberhello on 13/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ActionView: UIView {
    
    override var isHidden: Bool {
        didSet {
            if isHidden {
                heightLayoutConstraint?.constant = 0
            }
        }
    }

    @IBOutlet weak var actionButton: RoundButton!
    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint?
    
    func setAction(work: Work) {
        actionButton.setTitle(work.getReadableWorkAction(), for: .normal)
        DispatchQueue.main.async {
            switch work.status {
            case WorkStatus.WAITING_CONFIRM_WORK.status:
                self.actionButton.backgroundColor = UIColor(named: "Tint Color")
            default:
                self.actionButton.backgroundColor = UIColor(named: "Primary Color")
            }
        }
    }

}
