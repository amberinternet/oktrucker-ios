//
//  AdditionalServiceView.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class AdditionalServiceView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: IntrinsicTableView!
    var additionalServiceList: [AdditionalService]!
    
    func initView(additionalServiceList: [AdditionalService]) {
        tableView.delegate = self
        tableView.dataSource = self
        self.additionalServiceList = additionalServiceList
    }
    
    // TableView dataSource & delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return additionalServiceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalServiceCell", for: indexPath) as! AdditionalServiceViewCell
        cell.label.text = additionalServiceList[indexPath.item].name
        return cell
    }

}
