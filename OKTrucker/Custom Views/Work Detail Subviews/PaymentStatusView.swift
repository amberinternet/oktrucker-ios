//
//  PaymentStatusView.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class PaymentStatusView: UIView {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func initView(work: Work) {
        DispatchQueue.main.async {
            switch work.status {
            case WorkStatus.COMPLETE.status:
                if work.driverPaid == 1 {
                    self.setReceivedMoney()
                } else {
                    switch work.paymentMethod {
                    case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
                        self.setReceivedMoney()
                    default:
                        self.setWaitingConfirmCredit()
                    }
                }
            default:
                break
            }
        }
    }
    
    private func setReceivedMoney() {
        self.backgroundColor = UIColor(named: "Payment Success Color")
        self.label.textColor = UIColor.white
        self.icon.image = UIImage(named: "Checkbox Icon")
        self.label.text = NSLocalizedString("รับเงินแล้ว", comment: "")
    }
    
    private func setWaitingConfirmCredit() {
        self.backgroundColor = UIColor(named: "Payment Pending Color")
        self.label.textColor = UIColor(named: "Text Color")
        self.icon.image = UIImage(named: "Clock Icon")
        self.label.text = NSLocalizedString("รอแอดมินเพิ่มเครดิต", comment: "")
    }

}
