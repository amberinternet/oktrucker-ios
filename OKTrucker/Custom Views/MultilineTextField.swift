//
//  MultilineTextField.swift
//  OKTrucker
//
//  Created by amberhello on 20/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class MultilineTextField: UITextView {
    
    @IBInspectable var multilineTextColor: UIColor? {
        didSet{
            if !isNilOrEmpty {
                textColor = multilineTextColor
            } else {
                textColor = tintColor
            }
        }
    }
    
    @IBInspectable var placeholder: String? {
        didSet{
            if text.isEmpty {
                text = placeholder
                textColor = tintColor
            }
        }
    }
    
    var isNilOrEmpty: Bool {
        if text == placeholder || text.isEmpty { return true }
        else { return false }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        self.delegate = self
    }
    
}

extension MultilineTextField: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            DispatchQueue.main.async {
                let beginningPosition = textView.beginningOfDocument
                textView.selectedTextRange = textView.textRange(from: beginningPosition, to: beginningPosition)
            }
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.text = textView.text.replacingOccurrences(of: placeholder ?? "", with: "")
        textView.textColor = multilineTextColor
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = tintColor
        }
    }
    
}

