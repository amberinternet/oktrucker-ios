//
//  AutocompleteTextField.swift
//  OKTrucker
//
//  Created by amberhello on 26/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

@IBDesignable class AutocompleteTextField: DesignableTextField {

    private var data: [String]!
    let dropDown = DropDown()
    var selectedIndex: Index? { return dropDown.indexForSelectedRow }
    private(set) var selectedId: Int? = nil
    private(set) var selectedItem: String? = nil
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.delegate = self
        
        DropDown.appearance().cellHeight = self.frame.height
        DropDown.appearance().textFont = font ?? UIFont.systemFont(ofSize: 16)
        
        dropDown.anchorView = self
        dropDown.direction = .any
        dropDown.topOffset = CGPoint(x: 0, y: -self.frame.height)
        dropDown.bottomOffset = CGPoint(x: 0, y: self.frame.height)
        dropDown.width = self.frame.width
        dropDown.backgroundColor = .white
    }
    
    func initDropDown(data: [String], selectionAction: @escaping (_ id: Int, _ item: String) -> Void = {_,_ in }) {
        self.data = data
        self.text = data[0]
        
        dropDown.dataSource = data
        dropDown.selectionAction = { (index: Int, item: String) in
            self.text = item
            self.selectedId = index + 1
            self.selectedItem = item
            selectionAction(self.selectedId!, self.selectedItem!)
        }
        
    }
    
    func setSelected(item: String) {
        selectedItem = item
        text = item
    }

}

extension AutocompleteTextField /* UITextFieldDelegate */ {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        dropDown.show()
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let searchText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if searchText.isEmpty {
            dropDown.dataSource = data
        } else {
            dropDown.dataSource = data.filter { $0.localizedCaseInsensitiveContains(searchText) }
        }
        dropDown.show()
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isNilOrEmpty {
            dropDown.dataSource = data
            selectedItem = nil
        } else if textField.text != selectedItem {
            textField.text = ""
            selectedItem = nil
        }
    }
    
}
