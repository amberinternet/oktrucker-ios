//
//  TransactionRequestStatusLabel.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class TransactionRequestStatusLabel: StatusLabel {
    
    func initView(deposit request: Deposit) {
        text = request.getReadableStatus()
        set(status: request.status)
    }
    
    func initView(withdraw request: Withdraw) {
        text = request.getReadableStatus()
        set(status: request.status)
    }
    
    private func set(status: Int) {
        DispatchQueue.main.async {
            switch status {
            case TransactionRequestStatus.WAITING.status:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = UIColor(named: "Waiting Color")
                self.backgroundColor = UIColor.clear
            case TransactionRequestStatus.APPROVED.status:
                self.textColor = UIColor.white
                self.borderColor = UIColor(named: "Success Color")
                self.backgroundColor = UIColor(named: "Success Color")
            case TransactionRequestStatus.REJECT.status:
                self.textColor = UIColor.white
                self.borderColor = UIColor(named: "Reject Color")
                self.backgroundColor = UIColor(named: "Reject Color")
            default:
                break
            }
        }
    }
    
}

