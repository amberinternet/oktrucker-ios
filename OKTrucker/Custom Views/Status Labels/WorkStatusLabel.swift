//
//  WorkStatusLabel.swift
//  OKTrucker
//
//  Created by amberhello on 24/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class WorkStatusLabel: StatusLabel {
    
    func initView(work: Work) {
        text = work.getReadableWorkStatus()
        DispatchQueue.main.async {
            switch work.status {
            case WorkStatus.FIND_TRUCK.status:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.FIND_TRUCK.StatusColor
                self.backgroundColor = UIColor.clear
            case WorkStatus.WAITING_CONFIRM_WORK.status:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_CONFIRM_WORK.StatusColor
                self.backgroundColor = WorkStatus.WAITING_CONFIRM_WORK.StatusColor
            case WorkStatus.WAITING_RECEIVE_ITEM.status:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_RECEIVE_ITEM.StatusColor
                self.backgroundColor = WorkStatus.WAITING_RECEIVE_ITEM.StatusColor
            case WorkStatus.ARRIVED_SOURCE.status:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.ARRIVED_SOURCE.StatusColor
                self.backgroundColor = WorkStatus.ARRIVED_SOURCE.StatusColor
            case WorkStatus.WAITING_SENT_ITEM.status:
                self.textColor = UIColor.white
                self.borderColor = WorkStatus.WAITING_SENT_ITEM.StatusColor
                self.backgroundColor = WorkStatus.WAITING_SENT_ITEM.StatusColor
            case WorkStatus.ARRIVED_DESTINATION.status:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = WorkStatus.ARRIVED_DESTINATION.StatusColor
                self.backgroundColor = WorkStatus.ARRIVED_DESTINATION.StatusColor
            case WorkStatus.COMPLETE.status:
                if work.driverPaid == 1 {
                    self.setStatusFinish()
                } else {
                    switch work.status {
                    case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
                        self.setStatusFinish()
                    default:
                        self.textColor = UIColor(named: "Text Color")
                        self.borderColor =  WorkStatus.COMPLETE.StatusColor
                        self.backgroundColor = UIColor.clear
                    }
                }
            case WorkStatus.CANCEL.status:
                self.textColor = UIColor(named: "Text Color")
                self.borderColor = UIColor.clear
                self.backgroundColor = UIColor.clear
            default:
                break
            }
        }
    }
    
    private func setStatusFinish() {
        self.textColor = UIColor.white
        self.borderColor = WorkStatus.COMPLETE.StatusColor
        self.backgroundColor = WorkStatus.COMPLETE.StatusColor
    }
    
}
