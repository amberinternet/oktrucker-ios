//
//  DropDownView.swift
//  OKTrucker
//
//  Created by amberhello on 20/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

@IBDesignable class DropDownView: UIView {
    
    @IBInspectable var text: String? {
        didSet {
            setUp()
        }
    }
    
    @IBInspectable var textColor: UIColor! = UIColor(named: "Text Color") {
        didSet {
            setUp()
        }
    }
    
    @IBInspectable var font: UIFont! = UIFont.systemFont(ofSize: 16) {
        didSet {
            setUp()
        }
    }
    
    @IBInspectable var dropDownIcon: UIImage? = UIImage(named: "Caret Down Icon") {
        didSet {
            setUp()
        }
    }
    
    @IBInspectable var padding: CGFloat = 16 {
        didSet {
            setUp()
        }
    }
    
    // border
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }
    
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height:0))
    let dropDown = DropDown()
    var selectedIndex: Index? { return dropDown.indexForSelectedRow }
    var selectedId: Int? { if let index = selectedIndex { return index + 1 } else { return nil } }
    var selectedItem: String? { return dropDown.selectedItem }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUp()
    }
    
    private func setUp() {
        
        let imageSize = CGFloat(16)
        let labelWidth = self.frame.width-imageSize-(padding*2)-10
        let imageX = labelWidth+padding+10
        
        imageView.frame = CGRect(x: imageX, y: (self.frame.height-imageSize)/2, width: imageSize, height: imageSize)
//        imageView.backgroundColor = .green
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        
        if let image = dropDownIcon {
            imageView.image = image
        }
        
        label.frame = CGRect(x: padding, y: 0, width: labelWidth, height: self.frame.height)
//        label.backgroundColor = .blue
        label.text = text
        label.font = font
        addSubview(label)
        
        if let color = textColor {
            label.textColor = color
        }
        
        DropDown.appearance().cellHeight = self.frame.height
        DropDown.appearance().textFont = font
        
        dropDown.anchorView = self
        dropDown.backgroundColor = .white
        
    }
    
    func initDropDown(data: [String], selectionAction: @escaping (_ id: Int, _ item: String) -> Void = { _,_ in }) {
        dropDown.dataSource = data
        dropDown.selectionAction = { (index: Int, item: String) in
            self.label.text = item
            selectionAction(self.selectedId!, self.selectedItem!)
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showDropDownList))
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    func selectRow(at index: Index) {
        self.dropDown.selectRow(at: index)
        DispatchQueue.main.async {
            self.label.text = self.dropDown.selectedItem
        }
    }
    
    @objc private func showDropDownList() {
        dropDown.show()
    }
    
}
