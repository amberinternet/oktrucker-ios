//
//  CheckBox.swift
//  OKTrucker
//
//  Created by amberhello on 22/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class CheckBox: UIButton {
    
    var isChecked: Bool = false {
        didSet {
            if isChecked {
                self.setImage(UIImage(named: "Checkbox Image (Checked)")?.tintColor(color: tintColor), for: .normal)
            } else {
                self.setImage(UIImage(named: "Checkbox Image (Unchecked)")?.tintColor(color: tintColor), for: .normal)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override func prepareForInterfaceBuilder() {
        setUp()
    }
    
    private func setUp() {
        self.setImage(imageView?.image?.tintColor(color: tintColor), for: .normal)
        addTarget(self, action: #selector(check), for: .touchUpInside)
    }
    
    @objc private func check() {
        if isChecked { isChecked = false }
        else { isChecked = true }
    }
    
}
