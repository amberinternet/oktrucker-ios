//
//  CardExpiryMonthTextField.swift
//  OKTrucker
//
//  Created by amberhello on 5/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class CardExpiryMonthTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        self.delegate = self
    }
    
    func validate() -> Bool {
        guard let text = self.text, let month = Int(text) else {
            return false
        }
        if text.isEmpty || text.count < 2 {
            return false
        }
        if !(1...12 ~= month) {
            return false
        }
        return true
    }
    
}

extension CardExpiryMonthTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 2
    }
    
}
