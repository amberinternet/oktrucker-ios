//
//  PassedProvinceView.swift
//  OKTrucker
//
//  Created by amberhello on 9/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class PassedProvinceView: UIStackView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var provinceTextField: AutocompleteTextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        let nib = UINib(nibName: "PassedProvinceView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    func initView() {
        provinceTextField.initDropDown(data: Arrays.provinces)
        provinceTextField.text = ""
    }
    
    @IBAction func remove(_ sender: Any) {
        self.removeFromSuperview()
    }

}
