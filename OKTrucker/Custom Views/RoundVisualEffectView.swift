//
//  RoundVisualEffectView.swift
//  OKTrucker
//
//  Created by amberhello on 28/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class RoundVisualEffectView: UIVisualEffectView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
}

