//
//  TintImageView.swift
//  OKTrucker
//
//  Created by amberhello on 23/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

@IBDesignable class TintImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override func prepareForInterfaceBuilder() {
        setUp()
    }
    
    private func setUp() {
        self.image = self.image?.tintColor(color: tintColor)
    }
    
}
