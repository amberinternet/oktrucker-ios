//
//  Constant.swift
//  OK Trucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Foundation

/* init value */
const val INITIAL_STRING = ""
const val INITIAL_INT = 0
const val INITIAL_DOUBLE = 0.0
const val UNKNOWN = "ไม่ระบุ"
