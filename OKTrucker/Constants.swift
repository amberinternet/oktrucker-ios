//
//  Constant.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

struct Constants {
    /* init value */
    static let INITIAL_STRING = ""
    static let INITIAL_INT = 0
    static let INITIAL_DOUBLE = 0.0
    static let UNKNOWN = "ไม่ระบุ"
    
    static let INITIAL_YEAR = 2018
    
    /* network */
    static let TOKEN_TYPE = "Bearer "
    static let TOKEN_KEY = "token_key"
    
    enum HttpHeaderField: String {
        case authentication = "Authorization"
    }
    
    enum ContentType: String {
        case json = "application/json"
    }
    
    /* api */
    static let API_PATH = "api/v1/"
    
    static let PRODUCTION_URL = "https://backoffice.oktruck.com/"
    static let PRODUCTION_API_URL = PRODUCTION_URL + API_PATH
    
    static let BASE_URL = PRODUCTION_URL
    static let BASE_API_URL = PRODUCTION_API_URL
    
    /* user manager */
    static let IS_ENABLE_NOTIFICATION_KEY = "is_enable_notification_key"
    
    /* contact */
    static let ADDMIN_PHONE_NUMBER = "02-077-7498"
    
}
