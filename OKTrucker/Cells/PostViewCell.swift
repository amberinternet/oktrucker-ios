//
//  PostViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class PostViewCell: UICollectionViewCell {
    
    @IBOutlet weak var statusLabel: StatusLabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var sourceProvinceLabel: UILabel!
    @IBOutlet weak var destinationProvinceLabel: UILabel!
    @IBOutlet weak var passedProvincesLabel: UILabel!
    
    func initView(post: Post) {
        if post.status == 0 { statusLabel.isHidden = true }
        else { statusLabel.isHidden = false }
        sourceDateLabel.text = post.getReceiveDateTime()
        sourceProvinceLabel.text = post.sourceProvince
        destinationProvinceLabel.text = post.destinationProvince
        passedProvincesLabel.text = post.passedProvinces.joined(separator:" ")
    }
    
}
