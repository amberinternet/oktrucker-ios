//
//  TransactionViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 29/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TransactionViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func initView(transaction: Transaction) {
        DispatchQueue.main.async {
            self.dateLabel.text = transaction.createdAt.fromDateTime().toDateTimeAppFormat()
            self.nameLabel.attributedText = transaction.getReadableType()
            self.priceLabel.text = transaction.getReadableCredit()
            self.priceLabel.textColor = transaction.getCreditTextColor()
        }
    }
    
}
