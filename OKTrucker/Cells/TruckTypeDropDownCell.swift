//
//  TruckTypeDropDownCell.swift
//  OKTrucker
//
//  Created by amberhello on 21/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

class TruckTypeDropDownCell: DropDownCell {
    
    @IBOutlet weak var truckImageView: UIImageView!
    
}
