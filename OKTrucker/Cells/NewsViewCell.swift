//
//  NewsViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class NewsViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var shortContentLabel: UILabel!
    @IBOutlet weak var pinIcon: UIImageView!
    
    func initView(news: News) {
        titleLabel.text = news.title
        dateLabel.text = news.postAt.fromDateTime().toDateTimeAppFormat()
        shortContentLabel.text = news.shortContent
        if news.isPinned == 1 { pinIcon.isHidden = false } else { pinIcon.isHidden = true }
    }
    
}
