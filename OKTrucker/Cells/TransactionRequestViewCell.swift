//
//  TransactionRequestViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TransactionRequestViewCell: UITableViewCell {
    
    @IBOutlet weak var statusLabel: TransactionRequestStatusLabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    
    func initView(deposit request: Deposit) {
        dateLabel.text = request.createdAt.fromDateTime().toDateTimeAppFormat()
        nameLabel.text = request.readableId
        creditLabel.text = request.getReadableCredit()
        statusLabel.initView(deposit: request)
    }
    
    func initView(withdraw request: Withdraw) {
        dateLabel.text = request.createdAt.fromDateTime().toDateTimeAppFormat()
        nameLabel.text = request.readableId
        creditLabel.text = request.getReadableCredit()
        statusLabel.initView(withdraw: request)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = statusLabel.backgroundColor
        super.setSelected(selected, animated: animated)
        self.statusLabel.backgroundColor = color
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = statusLabel.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        self.statusLabel.backgroundColor = color
    }
    
}
