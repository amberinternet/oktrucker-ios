//
//  WorkViewCell.swift
//  OKTrucker
//
//  Created by amberhello on 24/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class WorkViewCell: UICollectionViewCell {
    
    @IBOutlet weak var statusLabel: WorkStatusLabel!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var workIdLabel: UILabel!
    @IBOutlet weak var sourceShortAddressLabel: UILabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var destinationShortAddressLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var paymentMethodIcon: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    func initView(work: Work) {
        truckTypeLabel.text = work.truckType.name
        workIdLabel.text = work.getReadableWorkId()
        sourceShortAddressLabel.text = work.source.getShortAddress()
        sourceDateLabel.text = work.source.getReceiveDateTime()
        destinationShortAddressLabel.text = work.destination.getShortAddress()
        destinationDateLabel.text = work.destination.getDeliveryDateTime()
        itemNameLabel.text = work.item.name
        priceLabel.text = work.getPrice()
        switch work.paymentMethod {
        case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
            paymentMethodIcon.image = UIImage(named: "Dollar Note Icon")
        default:
            paymentMethodIcon.image = UIImage(named: "Wallet Icon")
        }
        statusLabel.initView(work: work)
    }
    
}
