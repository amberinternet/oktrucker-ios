//
//  Arrays.swift
//  OKTrucker
//
//  Created by amberhello on 21/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

struct Arrays {
    
    static let deposit_amount_list = [
        "฿1000",
        "฿2000",
        "฿3000",
        "฿4000",
        "฿5000",
        "฿10000"
    ]
    
    static let transaction_request_types = [
        "คำขอเติมเงิน",
        "คำขอถอนเงิน"
    ]
    
    static let truck_type_names = [
        "4 ล้อคอก",
        "4 ล้อทึบ",
        "6 ล้อคอก",
        "6 ล้อทึบ",
        "6 ล้อเฮี๊ยบ",
        "10 ล้อคอก",
        "10 ล้อทึบ",
        "10 ล้อเฮี๊ยบ",
        "เทรเล่อร์"
    ]
    
    static let license_types = [
        "ท.2",
        "ท.3",
        "ท.4"
    ]
    
    static let driver_types = [
        "ผู้ขับอิสระ",
        "ผู้ขับสังกัดบริษัททั่วไป",
        "ผู้ขับสังกัด Contract Company"
    ]
    
    static let provinces = [
        "กระบี่",
        "กรุงเทพมหานคร",
        "กาญจนบุรี",
        "กาฬสินธุ์",
        "กำแพงเพชร",
        "ขอนแก่น",
        "จันทบุรี",
        "ฉะเชิงเทรา",
        "ชลบุรี",
        "ชัยนาท",
        "ชัยภูมิ",
        "ชุมพร",
        "เชียงราย",
        "เชียงใหม่",
        "ตรัง",
        "ตราด",
        "ตาก",
        "นครนายก",
        "นครปฐม",
        "นครพนม",
        "นครราชสีมา",
        "นครศรีธรรมราช",
        "นครสวรรค์",
        "นนทบุรี",
        "นราธิวาส",
        "น่าน",
        "บึงกาฬ",
        "บุรีรัมย์",
        "ปทุมธานี",
        "ประจวบคีรีขันธ์",
        "ปราจีนบุรี",
        "ปัตตานี",
        "พระนครศรีอยุธยา",
        "พะเยา",
        "พังงา",
        "พัทลุง",
        "พิจิตร",
        "พิษณุโลก",
        "เพชรบุรี",
        "เพชรบูรณ์",
        "แพร่",
        "ภูเก็ต",
        "มหาสารคาม",
        "มุกดาหาร",
        "แม่ฮ่องสอน",
        "ยโสธร",
        "ยะลา",
        "ร้อยเอ็ด",
        "ระนอง",
        "ระยอง",
        "ราชบุรี",
        "ลพบุรี",
        "เลย",
        "ลำปาง",
        "ลำพูน",
        "ศีรสะเกษ",
        "สกลนคร",
        "สงขลา",
        "สตูล",
        "สมุทรปราการ",
        "สมุทรสงคราม",
        "สมุทรสาคร",
        "สระแก้ว",
        "สระบุรี",
        "สิงห์บุรี",
        "สุโขทัย",
        "สุพรรณบุรี",
        "สุราษฎร์ธานี",
        "สุรินทร์",
        "หนองคาย",
        "หนองบัวลำภู",
        "อ่างทอง",
        "อำนาจเจริญ",
        "อุดรธานี",
        "อุตรดิตถ์",
        "อุทัยธานี",
        "อุบลราชธานี"
    ]
    
    static let filter_work_provinces = [
        "ทุกจังหวัด",
        "กระบี่,Krabi",
        "กรุงเทพมหานคร,Krung Thep Maha Nakhon,Bangkok",
        "กาญจนบุรี,Kanchanaburi",
        "กาฬสินธุ์,Kalasin",
        "กําแพงเพชร,Kamphaeng Phet",
        "ขอนแก่น,Khon Kaen",
        "จันทบุรี,Chanthaburi",
        "ฉะเชิงเทรา,Chachoengsao",
        "ชลบุรี,Chon Buri",
        "ชัยนาท,Chai Nat",
        "ชัยภูมิ,เมืองชัยภูมิ,Chaiyaphum",
        "ชุมพร,Chumphon",
        "เชียงราย,Chiang Rai",
        "เชียงใหม่,Chiang Mai",
        "ตรัง,Trang",
        "ตราด,Trat",
        "ตาก,Tak",
        "นครนายก,Nakhon Nayok",
        "นครปฐม,Nakhon Pathom",
        "นครพนม,Nakhon Phanom",
        "นครราชสีมา,Nakhon Ratchasima",
        "นครศรีธรรมราช,Nakhon Si Thammarat",
        "นครสวรรค์,Nakhon Sawan",
        "นนทบุรี,Nonthaburi",
        "นราธิวาส,Narathiwat",
        "น่าน,Nan",
        "บึงกาฬ,Bueng Kan",
        "บุรีรัมย์,Buriram",
        "ปทุมธานี,Pathum Thani",
        "ประจวบคีรีขันธ์,Prachuap Khiri Khan",
        "ปราจีนบุรี,Prachinburi",
        "ปัตตานี,Pattani",
        "พระนครศรีอยุธยา,อยุธยา,Phra Nakhon Si Ayutthaya,Ayutthaya",
        "พะเยา,Phayao",
        "พังงา,Phang-nga,Phang Nga",
        "พัทลุง,Phatthalung",
        "พิจิตร,Phichit",
        "พิษณุโลก,Phitsanulok",
        "เพชรบุรี,Phetchaburi",
        "เพชรบูรณ์,Phetchabun",
        "แพร่,Phrae",
        "ภูเก็ต,Phuket",
        "มหาสารคาม,Maha Sarakham",
        "มุกดาหาร,Mukdahan",
        "แม่ฮ่องสอน,Mae Hong Son",
        "ยโสธร,Yasothon",
        "ยะลา,Yala",
        "ร้อยเอ็ด,Roi Et",
        "ระนอง,Ranong",
        "ระยอง,Rayong",
        "ราชบุรี,Ratchaburi",
        "ลพบุรี,Lopburi",
        "เลย,Loei",
        "ลำปาง,Lampang",
        "ลำพูน,Lamphun",
        "ศรีสะเกษ,Sisaket",
        "สกลนคร,Sakon Nakhon",
        "สงขลา,Songkhla",
        "สตูล,Satun",
        "สมุทรปราการ,Samut Prakan",
        "สมุทรสงคราม,Samut Songkhram",
        "สมุทรสาคร,Samut Sakhon",
        "สระแก้ว,Sa Kaeo",
        "สระบุรี,Saraburi",
        "สิงห์บุรี,Sing Buri",
        "สุโขทัย,Sukhothai",
        "สุพรรณบุรี,Suphan Buri",
        "สุราษฎร์ธานี,Surat Thani",
        "สุรินทร์,Surin",
        "หนองคาย,Nong Khai",
        "หนองบัวลำภู,Nong Bua Lam Phu",
        "อ่างทอง,Ang Thong",
        "อำนาจเจริญ,Amnat Charoen",
        "อุดรธานี,Udon Thani",
        "อุตรดิตถ์,Uttaradit",
        "อุทัยธานี,Uthai Thani",
        "อุบลราชธานี,Ubon Ratchathani"
    ]
    
    static let hour_time_list = [
        "00:00",
        "01:00",
        "02:00",
        "03:00",
        "04:00",
        "05:00",
        "06:00",
        "07:00",
        "08:00",
        "09:00",
        "10:00",
        "11:00",
        "12:00",
        "13:00",
        "14:00",
        "15:00",
        "16:00",
        "17:00",
        "18:00",
        "19:00",
        "20:00",
        "21:00",
        "22:00",
        "23:00"
    ]
    
    static let duration_time_list = [
        "08:00 - 12:00",
        "12:00 - 17:00",
        "17:00 - 00:00",
        "00:00 - 08:00"
    ]
    
    static let day_list = [
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23",
        "24",
        "25",
        "26",
        "27",
        "28",
        "29",
        "30",
        "31"
    ]
    
    static let hour_list = [
        "00",
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23"
    ]
    
    static let minute_list = [
        "00",
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23",
        "24",
        "25",
        "26",
        "27",
        "28",
        "29",
        "30",
        "31",
        "32",
        "33",
        "34",
        "35",
        "36",
        "37",
        "38",
        "39",
        "40",
        "41",
        "42",
        "43",
        "44",
        "45",
        "46",
        "47",
        "48",
        "49",
        "50",
        "51",
        "52",
        "53",
        "54",
        "55",
        "56",
        "57",
        "58",
        "59"
    ]
    
}
