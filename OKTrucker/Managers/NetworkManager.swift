//
//  NetworkManager.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: SessionManager {
    
    static var isConnectingInternet: Bool { return NetworkReachabilityManager()?.isReachable ?? false }
    private let requestInterceptor = RequestInterceptor()
    
    private init() {
        super.init()
        adapter = requestInterceptor
        retrier = requestInterceptor
    }
    
    static private let instance = NetworkManager()
    
    static func `default`() -> NetworkManager {
        return instance
    }
    
}
