//
//  DialogManager.swift
//  OKTrucker
//
//  Created by amberhello on 31/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DialogManager {
    
    static private var viewControllor: UIViewController! {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    private init() {}
    
    static private let instance = DialogManager()
    
    static func `default`() -> DialogManager {
        return instance
    }
    
    func showContactAdmin() {
        let dialogController = ContactAdminDialogController(nibName: "ContactAdminDialog", bundle: nil)
        present(dialogController: dialogController)
    }
    
    func showAlertReceiveCash(paymentMethod: String, price: String, completion: @escaping () -> Void) {
        let dialogController = AlertReceiveCashDialogController(nibName: "AlertReceiveCashDialog", bundle: nil)
        dialogController.paymentMethod = paymentMethod
        dialogController.price = price
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showConfirmAcceptWork(work: Work, completion: @escaping () -> Void) {
        let dialogController = ConfirmAcceptWorkDialogController(nibName: "ConfirmAcceptWorkDialog", bundle: nil)
        dialogController.work = work
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showConfirmSignature(title: String, completion: @escaping (_ signatureImage: UIImage) -> Void) {
        let dialogController = ConfirmSignatureDialogController(nibName: "ConfirmSignatureDialog", bundle: nil)
        dialogController.title = title
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showConfirm(title: String, message: String, completion: @escaping () -> Void) {
        let dialogController = ConfirmDialogController(nibName: "ConfirmDialog", bundle: nil)
        dialogController.title = title
        dialogController.message = message
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showAlert(message: String) {
        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
        dialogController.message = message
        present(dialogController: dialogController)
    }
    
    func showAlert(title: String, message: String) {
        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
        dialogController.title = title
        dialogController.message = message
        present(dialogController: dialogController)
    }
    
    func showAlert(message: String, completion: @escaping () -> Void) {
        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
        dialogController.message = message
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showAlert(title: String, message: String, completion: @escaping () -> Void) {
        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
        dialogController.title = title
        dialogController.message = message
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showAlert(title: String, message: String, action: String, completion: @escaping () -> Void) {
        let dialogController = AlertDialogController(nibName: "AlertDialog", bundle: nil)
        dialogController.title = title
        dialogController.message = message
        dialogController.action = action
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showError(error: String) {
        let dialogController = ErrorDialogController(nibName: "ErrorDialog", bundle: nil)
        dialogController.message = error
        present(dialogController: dialogController)
    }
    
    func showSuccess(message: String, completion: @escaping () -> Void) {
        let dialogController = SuccessDialogController(nibName: "SuccessDialog", bundle: nil)
        dialogController.message = message
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showSuccess(title: String, message: String, completion: @escaping () -> Void) {
        let dialogController = SuccessDialogController(nibName: "SuccessDialog", bundle: nil)
        dialogController.title = title
        dialogController.message = message
        dialogController.completion = completion
        present(dialogController: dialogController)
    }
    
    func showRequestAccessDialog(title: String, message: String, settingsUrl: String) {
        let dialogController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("การตั้งต่า", comment: ""), style: .default) { (_) -> Void in
            if let settingsUrl = URL(string: settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("ยกเลิก", comment: ""), style: .cancel, handler: nil)
        
        dialogController.addAction(settingsAction)
        dialogController.addAction(cancelAction)
        
        present(dialogController: dialogController)
    }
    
    func showToast(message : String, duration: Double = 2.0) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        DialogManager.viewControllor.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
    private var activityIndicatorDialogController: ActivityIndicatorDialogController? = nil
    
    func showLoading() {
        if activityIndicatorDialogController == nil {
            activityIndicatorDialogController = ActivityIndicatorDialogController(nibName: "ActivityIndicatorDialog", bundle: nil)
            present(dialogController: activityIndicatorDialogController)
        }
    }
    
    func showLoading(message: String) {
        if activityIndicatorDialogController == nil {
            activityIndicatorDialogController = ActivityIndicatorDialogController(nibName: "ActivityIndicatorDialog", bundle: nil)
            activityIndicatorDialogController!.message = message
            present(dialogController: activityIndicatorDialogController)
        } else {
            activityIndicatorDialogController!.loadingLabel.text = message
        }
    }
    
    func hideLoading() {
        if activityIndicatorDialogController != nil {
            activityIndicatorDialogController!.dismiss(animated: false, completion: {
                self.activityIndicatorDialogController = nil
            })
        }
    }
    
    func present(dialogController: UIViewController!) {
        dialogController.providesPresentationContextTransitionStyle = true
        dialogController.definesPresentationContext = true
        dialogController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        dialogController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        DialogManager.viewControllor.present(dialogController, animated: false)
    }
    
}
