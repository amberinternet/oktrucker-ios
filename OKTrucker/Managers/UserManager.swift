//
//  UserManager.swift
//  OKTrucker
//
//  Created by amberhello on 30/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import KeychainSwift

class UserManager {
    
    private let keychain = KeychainSwift()              // the keychain to store sensitive data (token)
    private let userDefaults = UserDefaults.standard    // the userDefaults to store data (app setting)
    
    var token: String { return keychain.get(Constants.TOKEN_KEY) ?? Constants.INITIAL_STRING }
    var isEnableNotification: Bool { /* return userDefaults.bool(forKey: Constants.IS_ENABLE_NOTIFICATION_KEY) */ return true }
    
    private init() {}
    
    static let `default` = UserManager()
    
    func storeAccessToken(_ accessToken: AccessToken) {
        keychain.set(accessToken.token, forKey: Constants.TOKEN_KEY)
    }
    
    func hasToken() -> Bool {
        if (keychain.get(Constants.TOKEN_KEY) != nil) {
            return true
        } else {
            return false
        }
    }
    
//    func setEnableNotification(isEnable: Bool) {
//        userDefaults.set(isEnable, forKey: Constants.IS_ENABLE_NOTIFICATION_KEY)
//    }
    
    func destroyAllData() {
        keychain.clear()
        userDefaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        userDefaults.synchronize()
    }
    
}
