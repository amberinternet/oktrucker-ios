//
//  Withdraw.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Withdraw {
    
    let id : Int!
    let price : Double!
    let status : Int!
    let approvedTime : String!
    let remark : String!
    let transferSlip : String!
    let payTime : String!
    let readableId : String!
    let createdAt : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        price = json["price"].doubleValue
        status = json["status"].intValue
        approvedTime = json["approved_time"].stringValue
        remark = json["approved_note"].stringValue
        transferSlip = json["pay_slip_image"].stringValue
        payTime = json["pay_time"].stringValue
        readableId = json["formatted_withdraw_id"].stringValue
        createdAt = json["created_at"].stringValue
    }
    
    func getCredit() -> String { return price.roundDecimalFormat() }
    
    func getReadableCredit() -> String {
        let credit = getCredit()
        return "฿\(credit)"
    }
    
    func getReadableStatus() -> String {
        switch(status) {
        case TransactionRequestStatus.WAITING.status: return TransactionRequestStatus.WAITING.withdrawMessage
        case TransactionRequestStatus.APPROVED.status: return TransactionRequestStatus.APPROVED.withdrawMessage
        case TransactionRequestStatus.REJECT.status: return TransactionRequestStatus.REJECT.withdrawMessage
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableTransferDate() -> String {
        switch(status) {
        case TransactionRequestStatus.WAITING.status: return TransactionRequestStatus.WAITING.withdrawMessage
        case TransactionRequestStatus.APPROVED.status: return payTime.fromDateTime().toDateTimeAppFormat()
        case TransactionRequestStatus.REJECT.status: return TransactionRequestStatus.REJECT.withdrawMessage
        default: return Constants.UNKNOWN
        }
    }
    
    func getSlipImagePath() -> String {
        return "image/withdraw/pay-slip-image/" + transferSlip
    }
    
}

extension Withdraw: Equatable {
    static func == (lhs: Withdraw, rhs: Withdraw) -> Bool {
        return lhs.id == rhs.id &&
            lhs.price == rhs.price &&
            lhs.status == rhs.status &&
            lhs.approvedTime == rhs.approvedTime &&
            lhs.remark == rhs.remark &&
            lhs.transferSlip == rhs.transferSlip &&
            lhs.payTime == rhs.payTime &&
            lhs.readableId == rhs.readableId &&
            lhs.createdAt == rhs.createdAt
    }
}
