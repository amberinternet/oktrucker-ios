//
//  Destination.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Destination {
    
    let id : Int!
    let latitude : String!
    let longitude : String!
    let address : String!
    let subDistrict : String!
    let district : String!
    let province : String!
    let contactName : String!
    let contactTelephoneNumber : String!
    let companyName : String!
    let landmark : String!
    let autograph : String!
    let date : String!
    let time : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        latitude = json["destination_latitude"].stringValue
        longitude = json["destination_longitude"].stringValue
        address = json["destination_full_address"].stringValue
        subDistrict = json["destination_address_subdistrict"].stringValue
        district = json["destination_address_district"].stringValue
        province = json["destination_address_province"].stringValue
        contactName = json["destination_contact_name"].stringValue
        contactTelephoneNumber = json["destination_contact_telephone_number"].stringValue
        companyName = json["destination_shop_name"].stringValue
        landmark = json["destination_landmark"].stringValue
        autograph = json["autograph"].stringValue
        date = json["delivered_date"].stringValue
        time = json["delivered_time"].stringValue
    }
    
    func getShortAddress() -> String {
        return district + ", " + province
    }
    
    func getFullAddress() -> String {
        return address
    }
    
    func getReadableTime() -> String {
        if(time == "เวลาใดก็ได้") { return time }
        else { return time + " น." }
    }
    
    func getDeliveryDateTime() -> String {
        return date.fromDate().toDateWithFullDayAppFormat() + " | " + getReadableTime()
    }
    
    func getReadableLandmark() -> String {
        return "จุดสังเกต: " + landmark
    }
    
    func getReadableContactName() -> String {
        return "ผู้ติดต่อ: " + contactName
    }
    
}

extension Destination: Equatable {
    static func == (lhs: Destination, rhs: Destination) -> Bool {
        return lhs.id == rhs.id &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude &&
            lhs.address == rhs.address &&
            lhs.subDistrict == rhs.subDistrict &&
            lhs.district == rhs.district &&
            lhs.province == rhs.province &&
            lhs.contactName == rhs.contactName &&
            lhs.contactTelephoneNumber == rhs.contactTelephoneNumber &&
            lhs.companyName == rhs.companyName &&
            lhs.landmark == rhs.landmark &&
            lhs.autograph == rhs.autograph &&
            lhs.date == rhs.date &&
            lhs.time == rhs.time
    }
}
