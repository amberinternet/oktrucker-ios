//
//  Truck.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Truck {
    
    var id : Int = Constants.INITIAL_INT
    var truckTypeId : Int = Constants.INITIAL_INT
    var licensePlate : String = Constants.INITIAL_STRING
    var truckProvince : String = Constants.INITIAL_STRING
    var truckYear : String = Constants.INITIAL_STRING
    var truckModel : String = Constants.INITIAL_STRING
    var truckBrand : String = Constants.INITIAL_STRING
    var latitude : String = Constants.INITIAL_STRING
    var longitude : String = Constants.INITIAL_STRING
    var truckType : TruckType = TruckType()
    
    init() {}
    
    init(json : JSON){
        id = json["id"].intValue
        truckTypeId = json["car_type_id"].intValue
        licensePlate = json["license_plate"].stringValue
        truckProvince = json["car_province"].stringValue
        truckYear = json["car_year"].stringValue
        truckModel = json["car_model"].stringValue
        truckBrand = json["car_brand"].stringValue
        latitude = json["latitude"].stringValue
        longitude = json["longitude"].stringValue
        truckType = TruckType(json: json["car_type"])
    }
    
}
