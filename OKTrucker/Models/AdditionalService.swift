//
//  AdditionalService.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct AdditionalService {
    
    let id : Int!
    let name : String!
    let charge : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["additional_service_name"].stringValue
        charge = json["price"].stringValue
    }
    
}

extension AdditionalService: Equatable {
    static func == (lhs: AdditionalService, rhs: AdditionalService) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.charge == rhs.charge
    }
}

