//
//  Item.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Item {
    
    let id : Int!
    let name : String!
    let quantity : String!
    let weight : String!
    let image : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["item_name"].stringValue
        quantity = json["item_quantity"].stringValue
        weight = json["item_weight"].stringValue
        image = json["item_image"].stringValue
    }
    
    func getItemImagePath() -> String {
        return "image/item/" + image
    }
    
}

extension Item: Equatable {
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.quantity == rhs.quantity &&
            lhs.weight == rhs.weight &&
            lhs.image == rhs.image
    }
}
