//
//  DriverType.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DriverType {
    
    var id : Int = Constants.INITIAL_INT
    var name : String = Constants.INITIAL_STRING
    var commission : Int = Constants.INITIAL_INT
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["driver_type_name"].stringValue
        commission = json["commission"].intValue
    }
    
    func getCommissionPercent() -> String {
        return String(commission) + "%"
    }
    
}
