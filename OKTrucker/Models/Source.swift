//
//  Source.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Source {
    
    let id : Int!
    let latitude : String!
    let longitude : String!
    let address : String!
    let subdistrict : String!
    let district : String!
    let province : String!
    let contactName : String!
    let contactTelephoneNumber : String!
    let companyName : String!
    let landmark : String!
    let autograph : String!
    let date : String!
    let time : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        latitude = json["source_latitude"].stringValue
        longitude = json["source_longitude"].stringValue
        address = json["source_full_address"].stringValue
        subdistrict = json["source_address_subdistrict"].stringValue
        district = json["source_address_district"].stringValue
        province = json["source_address_province"].stringValue
        contactName = json["source_contact_name"].stringValue
        contactTelephoneNumber = json["source_contact_telephone_number"].stringValue
        companyName = json["source_shop_name"].stringValue
        landmark = json["source_landmark"].stringValue
        autograph = json["autograph"].stringValue
        date = json["received_date"].stringValue
        time = json["received_time"].stringValue
    }
    
    func getShortAddress() -> String {
        return district + ", " + province
    }
    
    func getFullAddress() -> String {
        return address
    }
    
    func getReadableTime() -> String {
        if(time == "เวลาใดก็ได้") { return time }
        else { return time + " น." }
    }
    
    func getReceiveDateTime() -> String {
        return date.fromDate().toDateWithFullDayAppFormat() + " | " + getReadableTime()
    }
    
    func getReadableLandmark() -> String {
        return "จุดสังเกต: " + landmark
    }
    
    func getReadableContactName() -> String {
        return "ผู้ติดต่อ: " + contactName
    }
    
}

extension Source: Equatable {
    static func == (lhs: Source, rhs: Source) -> Bool {
        return lhs.id == rhs.id &&
            lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude &&
            lhs.address == rhs.address &&
            lhs.subdistrict == rhs.subdistrict &&
            lhs.district == rhs.district &&
            lhs.province == rhs.province &&
            lhs.contactName == rhs.contactName &&
            lhs.contactTelephoneNumber == rhs.contactTelephoneNumber &&
            lhs.companyName == rhs.companyName &&
            lhs.landmark == rhs.landmark &&
            lhs.autograph == rhs.autograph &&
            lhs.date == rhs.date &&
            lhs.time == rhs.time
    }
}

