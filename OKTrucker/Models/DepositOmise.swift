//
//  DepositOmise.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DepositOmise {
    
    let deposit : Deposit!
    let authorizeURI : String!
    
    init(json : JSON) {
        deposit = Deposit(json: json["topup"])
        authorizeURI = json["authorize_uri"].stringValue
    }
    
}
