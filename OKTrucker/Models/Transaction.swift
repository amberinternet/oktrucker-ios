//
//  Transaction.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Transaction {
    
    let id : Int!
    let type : Int!
    let description : String!
    let oldCreditBalance : String!
    let newCreditBalance : String!
    let credit : Double!
    let createdAt : String!
    let work : Work!
    let deposit : Deposit!
    let withdraw : Withdraw!
    
    init(json : JSON) {
        id = json["id"].intValue
        type = json["credit_log_type"].intValue
        description = json["description"].stringValue
        oldCreditBalance = json["old_credit_balance"].stringValue
        newCreditBalance = json["new_credit_balance"].stringValue
        credit = json["credit"].doubleValue
        createdAt = json["created_at"].stringValue
        work = Work(json: json["work"])
        deposit = Deposit(json: json["topup"])
        withdraw = Withdraw(json: json["withdraw"])
    }
    
    func getReadableType() -> NSAttributedString {
        var attributedString: NSMutableAttributedString
        
        switch(type) {
            
        case TransactionType.DEPOSIT.value:
            let string = "เติมเงิน เลขที่คำขอ " + deposit.readableId
            let range = (string as NSString).range(of: "เติมเงิน")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttributes([.font : UIFont.boldSystemFont(ofSize: 16)], range: range)
            
        case TransactionType.CANCEL_WORK.value:
            let string = "งานยกเลิก เลขที่งาน " + work.getReadableWorkId()
            let range = (string as NSString).range(of: "งานยกเลิก")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.ADMIN_ADD.value:
            let string = "เพิ่มโดยแอดมิน"
            let range = (string as NSString).range(of: "เพิ่ม")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.ADMIN_DELETE.value:
            let string = "หักโดยแอดมิน"
            let range = (string as NSString).range(of: "หัก")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.WITHDRAW.value:
            let string = "ถอนเงิน เลขที่คำขอ " + withdraw.readableId
            let range = (string as NSString).range(of: "ถอนเงิน")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.FINISH.value:
            let string = "เสร็จงาน เลขที่งาน " + work.getReadableWorkId()
            let range = (string as NSString).range(of: "เสร็จงาน")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        case TransactionType.ACCEPT_WORK.value:
            let string = "รับงาน เลขที่งาน " + work.getReadableWorkId()
            let range = (string as NSString).range(of: "รับงาน")
            attributedString = NSMutableAttributedString(string: string, attributes: [.font : UIFont.systemFont(ofSize: 16)])
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 16), range: range)
            
        default: attributedString = NSMutableAttributedString(string: Constants.UNKNOWN)
        }
        
        return attributedString
    }
    
    func getReadableCredit() -> String {
        let credit = self.credit.roundDecimalFormat()
        
        switch(type) {
        case TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.ADMIN_ADD.value, TransactionType.FINISH.value: return "+ ฿" + credit
        case TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value, TransactionType.ACCEPT_WORK.value: return "- ฿" + credit
        default: return "฿" + credit
        }
    }
    
    func getCreditTextColor() -> UIColor {
        switch(type) {
        case TransactionType.DEPOSIT.value, TransactionType.CANCEL_WORK.value, TransactionType.ADMIN_ADD.value, TransactionType.FINISH.value: return UIColor(named: "Deposit Color")!
        case TransactionType.ADMIN_DELETE.value, TransactionType.WITHDRAW.value, TransactionType.ACCEPT_WORK.value: return UIColor(named: "Withdraw Color")!
        default: return UIColor(named: "Text Color")!
        }
    }
}

extension Transaction: Equatable {
    static func == (lhs: Transaction, rhs: Transaction) -> Bool {
        return lhs.id == rhs.id &&
            lhs.type == rhs.type &&
            lhs.description == rhs.description &&
            lhs.oldCreditBalance == rhs.oldCreditBalance &&
            lhs.newCreditBalance == rhs.newCreditBalance &&
            lhs.credit == rhs.credit &&
            lhs.createdAt == rhs.createdAt &&
            lhs.work == rhs.work &&
            lhs.deposit == rhs.deposit &&
            lhs.withdraw == rhs.withdraw
    }
}
