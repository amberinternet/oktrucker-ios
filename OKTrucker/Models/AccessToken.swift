//
//  AccessToken.swift
//  OKTrucker
//
//  Created by amberhello on 25/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct AccessToken {
    
    let token : String!
 
    init(json : JSON) {
        token = json["token"].stringValue
    }
    
}
