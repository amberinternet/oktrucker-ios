//
//  Driver.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Driver {
    
    var id : Int = Constants.INITIAL_INT
    var driverTypeId : Int = Constants.INITIAL_INT
    var companyName : String = Constants.INITIAL_STRING
    var citizenId : String = Constants.INITIAL_STRING
    var driverLicenseId : String = Constants.INITIAL_STRING
    var driverLicenseType : String = Constants.INITIAL_STRING
    var createdAt : String = Constants.INITIAL_STRING
    var readableDriverId : String = Constants.INITIAL_STRING
    var bankAccount : BankAccount = BankAccount()
    var driverType : DriverType = DriverType()
    var truck = Truck()
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        driverTypeId = json["driver_type_id"].intValue
        companyName = json["company_name"].stringValue
        citizenId = json["citizen_id"].stringValue
        driverLicenseId = json["driver_license_id"].stringValue
        driverLicenseType = json["driver_license_type"].stringValue
        createdAt = json["created_at"].stringValue
        readableDriverId = json["formatted_driver_id"].stringValue
        bankAccount = BankAccount(json: json["account"])
        driverType = DriverType(json: json["type"])
        truck = Truck(json: json["car"])
    }
    
}
