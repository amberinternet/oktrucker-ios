//
//  TruckType.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct TruckType {
    
    var id : Int = Constants.INITIAL_INT
    var name : String = Constants.INITIAL_STRING
    var imageURI : String = Constants.INITIAL_STRING
    var description : String = Constants.INITIAL_STRING
    var priceFirst3km : String = Constants.INITIAL_STRING
    var price3To15km : String = Constants.INITIAL_STRING
    var price15To100km : String = Constants.INITIAL_STRING
    var price100UpKm : String = Constants.INITIAL_STRING
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        name = json["car_type_name"].stringValue
        imageURI = json["car_type_image"].stringValue
        description = json["car_type_description"].stringValue
        priceFirst3km = json["price_for_first_3_km"].stringValue
        price3To15km = json["price_per_km_at_3_15"].stringValue
        price15To100km = json["price_per_km_at_15_100"].stringValue
        price100UpKm = json["price_per_km_at_100up"].stringValue
    }
    
    init(id :Int, name: String, imageURI: String) {
        self.id = id
        self.name = name
        self.imageURI = imageURI
    }
    
}

extension TruckType: Equatable {
    static func == (lhs: TruckType, rhs: TruckType) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.imageURI == rhs.imageURI &&
            lhs.description == rhs.description &&
            lhs.priceFirst3km == rhs.priceFirst3km &&
            lhs.price3To15km == rhs.price3To15km &&
            lhs.price15To100km == rhs.price15To100km &&
            lhs.price100UpKm == rhs.price100UpKm
    }
}
