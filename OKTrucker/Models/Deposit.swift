//
//  Deposit.swift
//  OKTrucker
//
//  Created by amberhello on 25/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Deposit {
    
    let id : Int!
    let type : Int!
    let price : Double!
    let status : Int!
    let approvedTime : String!
    let remark : String!
    let transferSlip : String!
    let payTime : String!
    let readableId : String!
    let createdAt : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        type = json["topup_type"].intValue
        price = json["price"].doubleValue
        status = json["status"].intValue
        approvedTime = json["approved_time"].stringValue
        remark = json["approved_note"].stringValue
        transferSlip = json["pay_slip_image"].stringValue
        payTime = json["pay_time"].stringValue
        readableId = json["formatted_topup_id"].stringValue
        createdAt = json["created_at"].stringValue
    }
    
    func getCredit() -> String { return price.roundDecimalFormat() }
    
    func getReadableCredit() -> String {
        let credit = getCredit()
        return "฿\(credit)"
    }
    
    func getReadableStatus() -> String {
        switch(status) {
        case TransactionRequestStatus.APPROVED.status: return TransactionRequestStatus.APPROVED.depositMessage
        case TransactionRequestStatus.WAITING.status: return TransactionRequestStatus.WAITING.depositMessage
        case TransactionRequestStatus.REJECT.status: return TransactionRequestStatus.REJECT.depositMessage
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadablePaymentMethod() -> String {
        switch(type) {
        case DepositType.OMISE.value: return DepositType.OMISE.method
        case DepositType.TRANSFER.value: return DepositType.TRANSFER.method
        default: return Constants.UNKNOWN
        }
    }
    
    func getSlipImagePath() -> String {
        return "image/topup/pay-slip-image/" + transferSlip
    }
    
}

extension Deposit: Equatable {
    static func == (lhs: Deposit, rhs: Deposit) -> Bool {
        return lhs.id == rhs.id &&
            lhs.type == rhs.type &&
            lhs.price == rhs.price &&
            lhs.status == rhs.status &&
            lhs.approvedTime == rhs.approvedTime &&
            lhs.remark == rhs.remark &&
            lhs.transferSlip == rhs.transferSlip &&
            lhs.payTime == rhs.payTime &&
            lhs.readableId == rhs.readableId &&
            lhs.createdAt == rhs.createdAt
    }
}
