//
//  Work.swift
//  OKTrucker
//
//  Created by amberhello on 27/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Work {
    
    let id : Int!
    let commission : Int!
    let distance : Int!
    let distancePrice : String!
    let fullPrice : Double!
    var status : Int!
    let paymentMethod : Int!
    let userFullName : String!
    let userTelephoneNumber : String!
    let remark : String!
    let driverPaid : Int!
    let formattedWorkId : String!
    let commissionPrice : Double!
    let summaryPrice : Double!
    let item : Item!
    let truckType : TruckType!
    var additionalServiceList : [AdditionalService]! = []
    let source : Source!
    let destination : Destination!
    
    init(json : JSON){
        id = json["id"].intValue
        commission = json["commission"].intValue
        distance = json["distance"].intValue
        distancePrice = json["distance_price"].stringValue
        fullPrice = json["full_price"].doubleValue
        status = json["status"].intValue
        paymentMethod = json["payment_method"].intValue
        userFullName = json["user_name"].stringValue
        userTelephoneNumber = json["user_telephone_number"].stringValue
        remark = json["user_note"].stringValue
        driverPaid = json["is_driver_paid"].intValue
        formattedWorkId = json["formatted_work_id"].stringValue
        commissionPrice = json["commission_price"].doubleValue
        summaryPrice = json["summary_price"].doubleValue
        item = Item(json: json["item"])
        truckType = TruckType(json: json["car_type"])
        if let jsonArray = json["additional_services"].array {
            for jsonObject in jsonArray {
                let additionalService = AdditionalService(json: jsonObject)
                additionalServiceList.append(additionalService)
            }
        }
        source = Source(json: json["source"])
        destination = Destination(json: json["destination"])
    }
    
    func getReadableWorkId() -> String {
        return "#" + formattedWorkId
    }
    
    func getReadableDistance() -> String {
        let km = Double(distance) / 1000.0
        return km.roundDecimalFormat() + " กม."
    }
    
    func getReadablePaymentMethod() -> String {
        switch(paymentMethod) {
        case PaymentMethod.CASH_SOURCE.status: return PaymentMethod.CASH_SOURCE.pay
        case PaymentMethod.CASH_DESTINATION.status: return PaymentMethod.CASH_DESTINATION.pay
        case PaymentMethod.WALLET.status: return PaymentMethod.WALLET.pay
        case PaymentMethod.OMISE.status: return PaymentMethod.OMISE.pay
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableReceiveMethod() -> String {
        switch(paymentMethod) {
        case PaymentMethod.CASH_SOURCE.status: return PaymentMethod.CASH_SOURCE.receive
        case PaymentMethod.CASH_DESTINATION.status: return PaymentMethod.CASH_DESTINATION.receive
        case PaymentMethod.WALLET.status, PaymentMethod.OMISE.status: return PaymentMethod.WALLET.receive
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableWorkStatus() -> String {
        switch(status) {
        case WorkStatus.PAYMENT_FAIL.status: return WorkStatus.PAYMENT_FAIL.readableStatus
        case WorkStatus.FIND_TRUCK.status: return WorkStatus.FIND_TRUCK.readableStatus
        case WorkStatus.WAITING_CONFIRM_WORK.status: return WorkStatus.WAITING_CONFIRM_WORK.readableStatus
        case WorkStatus.WAITING_RECEIVE_ITEM.status: return WorkStatus.WAITING_RECEIVE_ITEM.readableStatus
        case WorkStatus.ARRIVED_SOURCE.status: return WorkStatus.ARRIVED_SOURCE.readableStatus
        case WorkStatus.WAITING_SENT_ITEM.status: return WorkStatus.WAITING_SENT_ITEM.readableStatus
        case WorkStatus.ARRIVED_DESTINATION.status: return WorkStatus.ARRIVED_DESTINATION.readableStatus
        case WorkStatus.COMPLETE.status: return WorkStatus.COMPLETE.readableStatus
        case WorkStatus.CANCEL.status: return WorkStatus.CANCEL.readableStatus
        default: return Constants.UNKNOWN
        }
    }
    
    func getReadableWorkAction() -> String {
        switch(status) {
        case WorkStatus.PAYMENT_FAIL.status: return WorkStatus.PAYMENT_FAIL.readableAction
        case WorkStatus.FIND_TRUCK.status: return WorkStatus.FIND_TRUCK.readableAction
        case WorkStatus.WAITING_CONFIRM_WORK.status: return WorkStatus.WAITING_CONFIRM_WORK.readableAction
        case WorkStatus.WAITING_RECEIVE_ITEM.status: return WorkStatus.WAITING_RECEIVE_ITEM.readableAction
        case WorkStatus.ARRIVED_SOURCE.status: return WorkStatus.ARRIVED_SOURCE.readableAction
        case WorkStatus.WAITING_SENT_ITEM.status: return WorkStatus.WAITING_SENT_ITEM.readableAction
        case WorkStatus.ARRIVED_DESTINATION.status: return WorkStatus.ARRIVED_DESTINATION.readableAction
        case WorkStatus.COMPLETE.status: return WorkStatus.COMPLETE.readableAction
        case WorkStatus.CANCEL.status: return WorkStatus.CANCEL.readableAction
        default: return Constants.UNKNOWN
        }
    }
    
    func getPrice() -> String {
        return "฿" + summaryPrice.roundDecimalFormat()
    }
    
    func getFee() -> String {
        let commission = Double(User.shared.driver.driverType.commission)
        let fee = (commission * summaryPrice) / 100
        return "฿" + fee.roundDecimalFormat()
    }
    
    func getFinalWage() -> String {
        let commission = Double(User.shared.driver.driverType.commission)
        let wage = summaryPrice - ((commission * summaryPrice) / 100)
        return "฿" + wage.roundDecimalFormat()
    }
    
}

extension Work: Equatable {
    static func == (lhs: Work, rhs: Work) -> Bool {
        return lhs.id == rhs.id &&
            lhs.distance == rhs.distance &&
            lhs.commission == rhs.commission &&
            lhs.distancePrice == rhs.distancePrice &&
            lhs.summaryPrice == rhs.summaryPrice &&
            lhs.status == rhs.status &&
            lhs.paymentMethod == rhs.paymentMethod &&
            lhs.userFullName == rhs.userFullName &&
            lhs.userTelephoneNumber == rhs.userTelephoneNumber &&
            lhs.remark == rhs.remark &&
            lhs.driverPaid == rhs.driverPaid &&
            lhs.formattedWorkId == rhs.formattedWorkId &&
            lhs.commissionPrice == rhs.commissionPrice &&
            lhs.item == rhs.item &&
            lhs.truckType == rhs.truckType &&
            lhs.additionalServiceList == rhs.additionalServiceList &&
            lhs.source == rhs.source &&
            lhs.destination == rhs.destination
    }
}
