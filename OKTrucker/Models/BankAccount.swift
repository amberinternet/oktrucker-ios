//
//  BankAccount.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct BankAccount {
    
    var id : Int = Constants.INITIAL_INT
    var driverId : Int = Constants.INITIAL_INT
    var accountName : String = Constants.INITIAL_STRING
    var bank : String = Constants.INITIAL_STRING
    var accountNumber : String = Constants.INITIAL_STRING
    var accountImageURI : String = Constants.INITIAL_STRING
    
    init() {}
    
    init(json : JSON) {
        id = json["id"].intValue
        driverId = json["driver_id"].intValue
        accountName = json["account_name"].stringValue
        bank = json["account_bank"].stringValue
        accountNumber = json["account_number"].stringValue
        accountImageURI = json["account_image"].stringValue
    }
    
}
