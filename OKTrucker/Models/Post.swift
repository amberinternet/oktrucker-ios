//
//  Post.swift
//  OKTrucker
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct Post {
    
    let id : Int!
    let driverId : Int!
    let workId : Int!
    let status : Int!
    var sourceProvince : String = Constants.INITIAL_STRING
    var receivedDate : String = Constants.INITIAL_STRING
    var receivedTime : String = Constants.INITIAL_STRING
    var destinationProvince : String = Constants.INITIAL_STRING
    var deliveredDate : String = Constants.INITIAL_STRING
    var deliveredTime : String = Constants.INITIAL_STRING
    var remark : String = Constants.INITIAL_STRING
    let formattedPostId : String!
    var passedProvinces : [String] = []
    let work : Work?
    
    init() {
        id = Constants.INITIAL_INT
        driverId = Constants.INITIAL_INT
        workId = Constants.INITIAL_INT
        status = Constants.INITIAL_INT
        formattedPostId = Constants.INITIAL_STRING
        work = nil
    }
    
    init(json : JSON){
        id = json["id"].intValue
        driverId = json["driver_id"].intValue
        workId = json["work_id"].intValue
        status = json["status"].intValue
        sourceProvince = json["source_province"].stringValue
        receivedDate = json["received_date"].stringValue
        receivedTime = json["received_time"].stringValue
        destinationProvince = json["destination_province"].stringValue
        deliveredDate = json["delivered_date"].stringValue
        deliveredTime = json["delivered_time"].stringValue
        remark = json["post_remark"].stringValue
        formattedPostId = json["formatted_post_id"].stringValue
        if let jsonArray = json["passed_provinces"].array {
            for jsonObject in jsonArray {
                let province = jsonObject.stringValue
                passedProvinces.append(province)
            }
        }
        if json["work"] == JSON.null { work = nil }
        else { work = Work(json: json["work"]) }
    }
    
    func getReadable(time: String) -> String {
        if(time == "เวลาใดก็ได้") { return time }
        else { return time + " น." }
    }
    
    func getReceiveDateTime() -> String {
        return receivedDate.fromDate().toDateWithFullDayAppFormat() + " | " + getReadable(time: receivedTime)
    }
    
    func getDeliveryDateTime() -> String {
        return deliveredDate.fromDate().toDateWithFullDayAppFormat() + " | " + getReadable(time: deliveredTime)
    }
    
}

extension Post: Equatable {
    static func == (lhs: Post, rhs: Post) -> Bool {
        return lhs.id == rhs.id &&
            lhs.driverId == rhs.driverId &&
            lhs.workId == rhs.workId &&
            lhs.status == rhs.status &&
            lhs.sourceProvince == rhs.sourceProvince &&
            lhs.receivedDate == rhs.receivedDate &&
            lhs.receivedTime == rhs.receivedTime &&
            lhs.destinationProvince == rhs.destinationProvince &&
            lhs.deliveredDate == rhs.deliveredDate &&
            lhs.deliveredTime == rhs.deliveredTime &&
            lhs.remark == rhs.remark &&
            lhs.formattedPostId == rhs.formattedPostId &&
            lhs.passedProvinces == rhs.passedProvinces &&
            lhs.work == rhs.work
    }
}
