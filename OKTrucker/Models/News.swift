//
//  News.swift
//  OKTrucker
//
//  Created by amberhello on 26/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct News {
    
    let id : Int!
    let title : String!
    let shortContent : String!
    let fullContent : String!
    let isPinned : Int!
    let postAt : String!
    
    init(json : JSON) {
        id = json["id"].intValue
        title = json["title"].stringValue
        shortContent = json["short_content"].stringValue
        fullContent = json["full_content"].stringValue
        isPinned = json["is_pinned"].intValue
        postAt = json["posted_at"].stringValue
    }
    
}

extension News: Equatable {
    static func == (lhs: News, rhs: News) -> Bool {
        return lhs.id == rhs.id &&
            lhs.title == rhs.title &&
            lhs.shortContent == rhs.shortContent &&
            lhs.fullContent == rhs.fullContent &&
            lhs.isPinned == rhs.isPinned &&
            lhs.postAt == rhs.postAt
    }
}

