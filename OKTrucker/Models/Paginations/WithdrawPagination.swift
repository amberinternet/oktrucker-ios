//
//  WithdrawPagination.swift
//  OKTrucker

//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WithdrawPagination {
    
    var total : Int!
    var currentPage : Int!
    var withdrawList : [Withdraw]! = []
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let withdraw = Withdraw(json: jsonObject)
                withdrawList.append(withdraw)
            }
        }
    }
    
}

extension WithdrawPagination: Equatable {
    static func == (lhs: WithdrawPagination, rhs: WithdrawPagination) -> Bool {
        return lhs.withdrawList! == rhs.withdrawList!
    }
}
