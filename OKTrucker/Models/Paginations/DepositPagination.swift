//
//  DepositPagination.swift
//  OKTrucker
//
//  Created by amberhello on 30/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DepositPagination {
    
    var total : Int!
    var currentPage : Int!
    var depositList : [Deposit]! = []
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let deposit = Deposit(json: jsonObject)
                depositList.append(deposit)
            }
        }
    }
    
}

extension DepositPagination: Equatable {
    static func == (lhs: DepositPagination, rhs: DepositPagination) -> Bool {
        return lhs.depositList! == rhs.depositList!
    }
}
