//
//  TransactionPagination.swift
//  OKTrucker
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct TransactionPagination {
    
    var total : Int!
    var currentPage : Int!
    var transactionList : [Transaction]! = []
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let transaction = Transaction(json: jsonObject)
                transactionList.append(transaction)
            }
        }
    }
    
}

extension TransactionPagination: Equatable {
    static func == (lhs: TransactionPagination, rhs: TransactionPagination) -> Bool {
        return lhs.transactionList! == rhs.transactionList!
    }
}
