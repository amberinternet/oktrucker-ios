//
//  WorkPagination.swift
//  OKTrucker
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WorkPagination {
    
    var total : Int!
    var currentPage : Int!
    var workList : [Work]! = []
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let work = Work(json: jsonObject)
                workList.append(work)
            }
        }
    }
    
}

extension WorkPagination: Equatable {
    static func == (lhs: WorkPagination, rhs: WorkPagination) -> Bool {
        return lhs.workList! == rhs.workList!
    }
}
