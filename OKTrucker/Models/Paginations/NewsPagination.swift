//
//  NewsPagination.swift
//  OKTrucker
//
//  Created by amberhello on 2/5/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct NewsPagination {
    
    var total : Int!
    var currentPage : Int!
    var newsList : [News]! = []
    
    init(json : JSON) {
        total = json["total"].intValue
        currentPage = json["current_page"].intValue
        if let jsonArray = json["data"].array {
            for jsonObject in jsonArray {
                let new = News(json: jsonObject)
                newsList.append(new)
            }
        }
    }
    
}

extension NewsPagination: Equatable {
    static func == (lhs: NewsPagination, rhs: NewsPagination) -> Bool {
        return lhs.newsList! == rhs.newsList!
    }
}
