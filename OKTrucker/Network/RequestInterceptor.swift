//
//  RequestInterceptor.swift
//  OKTrucker
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RequestInterceptor: RequestAdapter, RequestRetrier {
    private typealias RefreshCompletion = (_ succeeded: Bool, _ token: String?) -> Void
    
    private let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return SessionManager(configuration: configuration)
    }()
    
    private let lock = NSLock()
    
    private var token: String
    
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    // MARK: - Initialization
    
    public init() {
        self.token = UserManager.default.token
        print("RequestInterceptor: start")
    }
    
    // MARK: - RequestAdapter
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(Constants.BASE_URL) {
            var urlRequest = urlRequest
            urlRequest.setValue(Constants.TOKEN_TYPE + UserManager.default.token, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
            return urlRequest
        }
        
        return urlRequest
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)
            
            if !isRefreshing {
                refreshTokens { [weak self] succeeded, token in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
                    
                    if let token = token {
                        strongSelf.token = token
                    }
                    
                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    // MARK: - Private - Refresh Tokens
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }
        
        isRefreshing = true
        
        sessionManager.request(UserAPIRouter.refreshToken)
            .responseJSON { [weak self] response in
                guard let strongSelf = self else { return }
                
                if
                    let value = response.result.value
                {
                    let json = JSON(value)
                    let response = AccessTokenResponse(json: json)
                    let token = response.accessToken.token
                    
                    UserManager.default.storeAccessToken(response.accessToken)
                    
                    completion(true, token)
                    print("RequestInterceptor: success")
                } else {
                    User.shared.clear()
                    UserManager.default.destroyAllData()
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationController")
                    
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        topController.present(nextViewController, animated:true, completion:nil)
                    } else {
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        appDelegate?.window?.rootViewController = nextViewController
                    }

                    completion(false, nil)
                    print("RequestInterceptor: failure")
                }
                
                strongSelf.isRefreshing = false
        }
    }
}
