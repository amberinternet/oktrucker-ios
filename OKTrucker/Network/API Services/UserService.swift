//
//  UserService.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire
import SwiftyJSON
import RxSwift

class UserService {
    
    private let networkManager = NetworkManager.default()
    
    private init() {}
    
    static let `default` = UserService()
    
    func refreshToken() -> Observable<AccessTokenResponse> {
        return Observable<AccessTokenResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.refreshToken).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AccessTokenResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func destroyToken(firebaseToken: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.destroyToken(firebaseToken)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getProfile() -> Observable<UserResponse> {
        return Observable<UserResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getProfile).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = UserResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getWork(page: Int, search: String, sort: String) -> Observable<WorkListResponse> {
        return Observable<WorkListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getWork(page, search, sort)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WorkListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getMyWork(page: Int, status: String, search: String, sort: String) -> Observable<WorkListResponse> {
        return Observable<WorkListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getMyWork(page, status, search, sort)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WorkListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getWorkDetail(workId: Int) -> Observable<WorkResponse> {
        return Observable<WorkResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getWorkDetail(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WorkResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func acceptWork(workId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.acceptWork(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func arrivedSource(workId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.arrivedSource(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func departSource(workId: Int, signatureData: Data) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(signatureData, withName: "autograph", fileName: "autograph.jpeg", mimeType: "image/jpeg")
            }, with: UserAPIRouter.departSource(workId)) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func arrivedDestination(workId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.arrivedDestination(workId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func completeWork(workId: Int, signatureData: Data) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(signatureData, withName: "autograph", fileName: "autograph.jpeg", mimeType: "image/jpeg")
            }, with: UserAPIRouter.completeWork(workId)) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func getNews(page: Int) -> Observable<NewsListResponse> {
        return Observable<NewsListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getNews(page)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = NewsListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getNewsDetail(newsId: Int) -> Observable<NewsResponse> {
        return Observable<NewsResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getNewsDetail(newsId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = NewsResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getTransaction(page: Int, date: String) -> Observable<TransactionsListResponse> {
        return Observable<TransactionsListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getTransaction(page, date)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = TransactionsListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func withdraw(price: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.withdraw(price)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func depositWithTransferSlip(type: Int, price: Int, payTime: String, paySlipData: Data) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            self.networkManager.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(type)".data(using: String.Encoding.utf8)!, withName: "topup_type")
                multipartFormData.append("\(price)".data(using: String.Encoding.utf8)!, withName: "price")
                multipartFormData.append("\(payTime)".data(using: String.Encoding.utf8)!, withName: "pay_time")
                multipartFormData.append(paySlipData, withName: "pay_slip_image", fileName: "pay_slip.jpeg", mimeType: "image/jpeg")
            }, with: UserAPIRouter.depositWithTransferSlip) { result in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        let value = response.result.value!
                        switch response.response!.statusCode {
                        case 200...299:
                            let data = SuccessResponse(json: JSON(value))
                            observer.onNext(data)
                            observer.onCompleted()
                        case 401:
                            self.logOut()
                            fallthrough
                        default:
                            let error = ErrorResponse(json: JSON(value))
                            observer.onError(error)
                        }
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create()
        }
    }
    
    func depositWithOmise(type: Int, price: Int, omiseToken: String, fromApp: Int) -> Observable<DepositOmiseResponse> {
        return Observable<DepositOmiseResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.depositWithOmise(type, price, omiseToken, fromApp)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = DepositOmiseResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func completePaymentOmise(depositId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.completePaymentOmise(depositId: depositId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getDeposit(page: Int, date: String) -> Observable<DepositListResponse> {
        return Observable<DepositListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getDeposit(page, date)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = DepositListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getWithdraw(page: Int, date: String) -> Observable<WithdrawListResponse> {
        return Observable<WithdrawListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getWithdraw(page, date)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = WithdrawListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func updateLocation(latitude: Double, longitude: Double) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.updateLocation(latitude, longitude)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func getMyPost(page: Int) -> Observable<PostListResponse> {
        return Observable<PostListResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.getMyPost(page)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = PostListResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func deletePost(postId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.deletePost(postId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func createPost(sourceProvince: String, receivedDate: String, receivedTime: String, destinationProvince: String, deliveredDate: String, deliveredTime: String, passedProvinces: String, remark: String) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(UserAPIRouter.createPost(sourceProvince, receivedDate, receivedTime, destinationProvince, deliveredDate, deliveredTime, passedProvinces, remark)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    case 401:
                        self.logOut()
                        fallthrough
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
}

extension UserService {
    
    func logOut() {
        User.shared.clear()
        UserManager.default.destroyAllData()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationController")
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(nextViewController, animated:true, completion:nil)
        } else {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = nextViewController
        }
    }
    
}
