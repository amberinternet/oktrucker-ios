//
//  AuthService.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire
import SwiftyJSON
import RxSwift

class AuthService {
    
    private let networkManager = NetworkManager.default()
    
    private init() {}
    
    static let `default` = AuthService()
    
    func logInWithTelephone(telephone: String, password: String, firebaseToken: String, type: String) -> Observable<AccessTokenResponse> {
        return Observable<AccessTokenResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.logInWithTelephone(telephone, password, firebaseToken, type)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = AccessTokenResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func signUp(
        firstName: String,
        lastName: String,
        username: String,
        email: String,
        password: String,
        confirmPassword: String,
        address: String,
        companyName: String,
        telephoneNumber: String,
        driverTypeId: Int,
        driverLicenseId: String,
        driverLicenseType: String,
        citizenId: String,
        truckTypeId: Int,
        licensePlate: String,
        truckProvince: String,
        truckYear: String,
        truckModel: String,
        truckBrand: String,
        logisteeId: Int) -> Observable<SuccessResponse> {
        return Observable<SuccessResponse>.create { observer in
            //Trigger the HttpRequest using AlamoFire (AF)
            let request = self.networkManager.request(AuthAPIRouter.signUp(firstName, lastName, username, email, password, confirmPassword, address, companyName, telephoneNumber, driverTypeId, driverLicenseId, driverLicenseType, citizenId, truckTypeId, licensePlate, truckProvince, truckYear, truckModel, truckBrand, logisteeId)).responseJSON { response in
                //Check the result from Alamofire's response and check if it's a success or a failure
                switch response.result {
                case .success(let value):
                    switch response.response!.statusCode {
                    case 200...299:
                        let data = SuccessResponse(json: JSON(value))
                        observer.onNext(data)
                        observer.onCompleted()
                    default:
                        let error = ErrorResponse(json: JSON(value))
                        observer.onError(error)
                    }
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            //Finally, we return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
}
