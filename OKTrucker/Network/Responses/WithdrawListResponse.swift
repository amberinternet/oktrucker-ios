//
//  WithdrawListResponse.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct WithdrawListResponse {
    
    let withdrawPagination : WithdrawPagination!
    
    init(json : JSON) {
        withdrawPagination = WithdrawPagination(json: json["data"])
    }
    
}
