//
//  PostListResponse.swift
//  OKTrucker
//
//  Created by amberhello on 7/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct PostListResponse {
    
    let postPagination : PostPagination!
    
    init(json : JSON) {
        postPagination = PostPagination(json: json["data"])
    }
    
}
