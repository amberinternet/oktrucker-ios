//
//  UserResponse.swift
//  OKTrucker
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct UserResponse {
    
    let user : User!
    
    init(json : JSON) {
        user = User(json: json["data"])
    }
    
}

