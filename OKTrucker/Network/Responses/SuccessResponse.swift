//
//  SuccessResponse.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct SuccessResponse {
    
    let message : String!
    
    init(json : JSON) {
        message = json["data"].stringValue
    }
    
}
