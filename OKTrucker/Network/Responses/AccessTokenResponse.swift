//
//  AccessTokenResponse.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct AccessTokenResponse {
    
    let accessToken : AccessToken!
    
    init(json : JSON) {
        accessToken = AccessToken(json: json["data"])
    }
    
}
