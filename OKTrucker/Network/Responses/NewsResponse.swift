//
//  NewsResponse.swift
//  OKTrucker
//
//  Created by amberhello on 17/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct NewsResponse {
    
    let news : News!
    
    init(json : JSON) {
        news = News(json: json["data"])
    }
    
}
