//
//  DepositOmiseResponse.swift
//  OKTrucker
//
//  Created by amberhello on 5/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DepositOmiseResponse {
    
    let depositOmise : DepositOmise!
    
    init(json : JSON) {
        depositOmise = DepositOmise(json: json["data"])
    }
    
}
