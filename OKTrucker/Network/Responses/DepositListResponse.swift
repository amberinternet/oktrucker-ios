//
//  DepositListResponse.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct DepositListResponse {
    
    let depositPagination : DepositPagination!
    
    init(json : JSON) {
        depositPagination = DepositPagination(json: json["data"])
    }
    
}
