//
//  ErrorResponse.swift
//  OKTrucker
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

struct ErrorResponse: LocalizedError {
    
    var message: String
    let description: ErrorDescription
    
    init(json : JSON) {
        message = json["ERROR"].stringValue
        if let error = json["error"].string {
            message = error
        }
        description = ErrorDescription(json: json["description"])
    }
    
    public var errorDescription: String?
    {
        if !(description.getError().isEmpty) {
            return description.getError()
        } else {
            switch message {
            case "validation_failed", "could_not_create_token": return "พบข้อผิดพลาด"
            case "invalid_credentials": return "เบอร์โทรศัพท์หรือรหัสผ่านไม่ถูกต้อง"
            case "token_invalid", "token_not_provided", "token_expired", "ERROR": return "กรุณาเข้าสู่ระบบใหม่"
            case "user_telephone_number_is_not_verified", "user_is_not_verified": return "บัญชีของคุณยังไม่ได้รับการยืนยัน\nกรุณารอแอดมินติดต่อกลับเพื่อยืนยันตัวตน"
            case "user_status_is_deactive": return "บัญชีของคุณถูกพักใช้งาน"
            case "user_credit_is_not_enough": return "จำนวนเครดิตไม่พอรับงาน"
            case "topup_not_found": return "ชำระเงินไม่สำเร็จ กรุณาติดต่อแอดมิน"
            case "credit_is_not_enough": return "เครดิตของคุณไม่เพียงพอ\nกรุณาเติมเงิน"
            case "news_not_found": return "ไม่พบข่าวสาร"
            case "work_not_found": return "ไม่พบงานที่คุณกำลังหา"
            case "work_cannot_cancel": return "ไม่สามารถยกเลิกงานได้"
            case "work_cannot_accept": return "ไม่สามารถรับงานได้"
            case "work_limit_exceeded": return "ไม่สามารถรับงานได้\nเนื่องจากคุณรับงานครบ 3 งานแล้ว"
            case "invalid_car_type": return "ไม่สามารถรับงานได้"
            case "work_cannot_reach_source": return "ไม่สามารถถึงต้นทางได้"
            case "too_early_to_reach_source": return "ยังไม่ถึงวันทำงาน ไม่สามารถถึงต้นทางได้"
            case "work_cannot_depart_from_source": return "ไม่สามารถรับสินค้าได้"
            case "work_cannot_reach_destination": return "ไม่สามารถถึงปลายทางได้"
            case "too_early_to_reach_destination": return "ยังไม่ถึงวันทำงาน ไม่สามารถถึงปลายทางได้"
            case "work_cannot_complete": return "ไม่สามารถส่งของได้"
            case "invalid_user_type": return "บัญชีของคุณไม่สามารถใช้งานแอปพลิเคชันคนขับรถได้"
            case "driver_cannot_post": return "ไม่สามารถโพสต์ได้\nเนื่องจากบัญชีของคุณยังไม่มีสิทธิ์โพสต์รถล่อง"
            case "post_limit_reached": return "ไม่สามารถโพสต์ได้\nเนื่องจากโพสต์ครบ 3 รายการแล้ว"
            case "received_date_must_be_in_7_days_period": return "โพสต์วันที่ออกเดินทางล่วงหน้าได้ไม่เกิน 7 วัน"
            case "delivered_date_is_invalid": return "โพสต์วันที่ถึงปลายทางล่วงหน้าได้ไม่เกิน 3 วัน\n(นับจากวันที่วันออกเดินทาง)"
            case "post_overlap": return "ไม่สามารถโพสต์ได้\nเนื่องจากวันที่ออกเดินทางซ้ำกับโพสต์อื่น"
            case "passed_province_duplicate_with_source": return "จังหวัดที่ผ่านซ้ำกับต้นทาง"
            case "passed_province_duplicate_with_destination": return "จังหวัดที่ผ่านซ้ำกับปลายทาง"
            case "passed_province_duplicate_with_itself": return "จังหวัดในเส้นทางผ่านซ้ำ"
            case "passed_province_must_be_less_than_or_equal_to_10": return "เส้นทางผ่านเกิน 10 จังหวัด"
            default: return message.replacingOccurrences(of: "_", with: " ")
            }
        }
    }
    
}

