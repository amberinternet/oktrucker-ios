//
//  ErrorDescription.swift
//  OKTrucker
//
//  Created by amberhello on 24/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import SwiftyJSON

class ErrorDescription {
    
    var firstNameList: [String] = []
    var lastNameList: [String] = []
    var usernameList: [String] = []
    var emailList: [String] = []
    var passwordList: [String] = []
    var addressList: [String] = []
    var driverTypeIdList: [String] = []
    var driverLicenseIdList: [String] = []
    var driverLicenseTypeList: [String] = []
    var citizenIdList: [String] = []
    var carTypeIdList: [String] = []
    var licensePlateList: [String] = []
    var carProvinceList: [String] = []
    var carYearList: [String] = []
    var carModelList: [String] = []
    var carBrandList: [String] = []
    var payTimeList: [String] = []
    var priceList: [String] = []
    var slipImageList: [String] = []
    var signatureList: [String] = []
    
    private var error: String = Constants.INITIAL_STRING
    
    init(json : JSON) {
        firstNameList = json["first_name"].arrayValue.map { $0.stringValue }
        lastNameList = json["last_name"].arrayValue.map { $0.stringValue }
        usernameList = json["username"].arrayValue.map { $0.stringValue }
        emailList = json["email"].arrayValue.map { $0.stringValue }
        passwordList = json["password"].arrayValue.map { $0.stringValue }
        addressList = json["current_address"].arrayValue.map { $0.stringValue }
        driverTypeIdList = json["driver_type_id"].arrayValue.map { $0.stringValue }
        driverLicenseIdList = json["driver_license_id"].arrayValue.map { $0.stringValue }
        driverLicenseTypeList = json["driver_license_type"].arrayValue.map { $0.stringValue }
        citizenIdList = json["citizen_id"].arrayValue.map { $0.stringValue }
        carTypeIdList = json["car_type_id"].arrayValue.map { $0.stringValue }
        licensePlateList = json["license_plate"].arrayValue.map { $0.stringValue }
        carProvinceList = json["car_province"].arrayValue.map { $0.stringValue }
        carYearList = json["car_year"].arrayValue.map { $0.stringValue }
        carModelList = json["car_model"].arrayValue.map { $0.stringValue }
        carBrandList = json["car_brand"].arrayValue.map { $0.stringValue }
        payTimeList = json["pay_time"].arrayValue.map { $0.stringValue }
        priceList = json["price"].arrayValue.map { $0.stringValue }
        slipImageList = json["pay_slip_image"].arrayValue.map { $0.stringValue }
        signatureList = json["autograph"].arrayValue.map { $0.stringValue }
    }
    
    func getError() -> String {
        error = Constants.INITIAL_STRING
        error += firstNameList.getError()
        error += lastNameList.getError()
        error += usernameList.getError()
        error += emailList.getError()
        error += passwordList.getError()
        error += payTimeList.getError()
        error += addressList.getError()
        error += driverTypeIdList.getError()
        error += driverLicenseIdList.getError()
        error += driverLicenseTypeList.getError()
        error += citizenIdList.getError()
        error += carTypeIdList.getError()
        error += licensePlateList.getError()
        error += carProvinceList.getError()
        error += carYearList.getError()
        error += carModelList.getError()
        error += carBrandList.getError()
        error += priceList.getError()
        error += slipImageList.getError()
        error += signatureList.getError()
        if !error.isEmpty { return error.trimmingCharacters(in: .whitespacesAndNewlines) } else { return error }
    }
    
}
