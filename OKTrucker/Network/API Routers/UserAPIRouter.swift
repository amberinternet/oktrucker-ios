//
//  UserAPIRouter.swift
//  OKTrucker
//
//  Created by amberhello on 7/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire

enum UserAPIRouter: URLRequestConvertible {
    //The endpoint name we'll call later
    case refreshToken
    case destroyToken(_ firebaseToken: String)
    case getProfile
    case getWork(_ page: Int, _ search: String, _ sort: String)
    case getMyWork(_ page: Int, _ status: String, _ search: String, _ sort: String)
    case getWorkDetail(_ workId: Int)
    case acceptWork(_ workId: Int)
    case arrivedSource(_ workId: Int)
    case departSource(_ workId: Int)
    case arrivedDestination(_ workId: Int)
    case completeWork(_ workId: Int)
    case getNews(_ page: Int)
    case getNewsDetail(_ newsId: Int)
    case getTransaction(_ page: Int, _ date: String)
    case withdraw(_ price: Int)
    case depositWithTransferSlip
    case depositWithOmise(_ type: Int, _ price: Int, _ omiseToken: String, _ fromApp: Int)
    case completePaymentOmise(depositId: Int)
    case getDeposit(_ page: Int, _ date: String)
    case getWithdraw(_ page: Int, _ date: String)
    case updateLocation(_ latitude: Double, _ longitude: Double)
    case getMyPost(_ page: Int)
    case deletePost(_ postId: Int)
    case createPost(_ sourceProvince: String, _ receivedDate: String, _ receivedTime: String, _ destinationProvince: String, _ deliveredDate: String, _ deliveredTime: String, _ passedProvinces: String, _ remark: String)
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .refreshToken:
            return .post
        case .destroyToken:
            return .post
        case .getProfile:
            return .get
        case .getWork:
            return .get
        case .getMyWork:
            return .get
        case .getWorkDetail:
            return .get
        case .acceptWork:
            return .post
        case .arrivedSource:
            return .post
        case .departSource:
            return .post
        case .arrivedDestination:
            return .post
        case .completeWork:
            return .post
        case .getNews:
            return .get
        case .getNewsDetail:
            return .get
        case .getTransaction:
            return .get
        case .withdraw:
            return .post
        case .depositWithTransferSlip:
            return .post
        case .depositWithOmise:
            return .post
        case .completePaymentOmise:
            return .post
        case .getDeposit:
            return .get
        case .getWithdraw:
            return .get
        case .updateLocation:
            return .post
        case .getMyPost:
            return .get
        case .deletePost:
            return .post
        case .createPost:
            return .post
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .refreshToken:
            return "token/refresh"
        case .destroyToken:
            return "token/destroy"
        case .getProfile:
            return "me"
        case .getWork:
            return "work/pending"
        case .getMyWork:
            return "work"
        case .getWorkDetail(let workId):
            return "work/\(workId)"
        case .acceptWork(let workId):
            return "work/\(workId)/accept"
        case .arrivedSource(let workId):
            return "work/\(workId)/reach-source"
        case .departSource(let workId):
            return "work/\(workId)/depart-from-source"
        case .arrivedDestination(let workId):
            return "work/\(workId)/reach-destination"
        case .completeWork(let workId):
            return "work/\(workId)/complete"
        case .getNews:
            return "news"
        case .getNewsDetail(let newsId):
            return "news/\(newsId)"
        case .getTransaction:
            return "me/credit-log"
        case .withdraw:
            return "me/withdraw"
        case .depositWithTransferSlip:
            return "me/topup"
        case .depositWithOmise:
            return "me/topup"
        case .completePaymentOmise(let depositId):
            return "topup/\(depositId)/success"
        case .getDeposit:
            return "me/topup"
        case .getWithdraw:
            return "me/withdraw"
        case .updateLocation:
            return "location"
        case .getMyPost:
            return "post/pending"
        case .deletePost(let postId):
            return "post/\(postId)/delete"
        case .createPost:
            return "post"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .refreshToken:
            return nil
        case .destroyToken(let firebaseToken):
            return ["token" : firebaseToken]
        case .getProfile:
            return nil
        case .getWork(let page, let search, let sort):
            return [
                "page" : page,
                "q" : search,
                "order_by" : sort
            ]
        case .getMyWork(let page, let status, let search, let sort):
            return [
                "page" : page,
                "status" : status,
                "q" : search,
                "order_by" : sort
            ]
        case .getWorkDetail:
            return nil
        case .acceptWork:
            return nil
        case .arrivedSource:
            return nil
        case .departSource:
            return nil
        case .arrivedDestination:
            return nil
        case .completeWork:
            return nil
        case .getNews(let page):
            return ["page" : page]
        case .getNewsDetail:
            return nil
        case .getTransaction(let page, let date):
            return [
                "page" : page,
                "month" : date
            ]
        case .withdraw(let price):
            return ["price" : price]
        case .depositWithTransferSlip:
            return nil
        case .depositWithOmise(let type, let price, let omiseToken, let fromApp):
            return [
                "topup_type" : type,
                "price" : price,
                "omise_token" : omiseToken,
                "from_app" : fromApp
            ]
        case .completePaymentOmise:
            return nil
        case .getDeposit(let page, let date):
            return [
                "page" : page,
                "q" : date
            ]
        case .getWithdraw(let page, let date):
            return [
                "page" : page,
                "q" : date
            ]
        case .updateLocation(let latitude, let longitude):
            return [
                "latitude" : latitude,
                "longitude" : longitude
            ]
        case .getMyPost(let page):
            return ["page" : page]
        case .deletePost:
            return nil
        case .createPost(let sourceProvince, let receivedDate, let receivedTime, let destinationProvince, let deliveredDate, let deliveredTime, let passedProvinces, let remark):
            return [
                "source_province" : sourceProvince,
                "received_date" : receivedDate,
                "received_time" : receivedTime,
                "destination_province" : destinationProvince,
                "delivered_date" : deliveredDate,
                "delivered_time" : deliveredTime,
                "provinces" : passedProvinces,
                "post_remark" : remark
            ]
        }
    }
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.BASE_API_URL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Authentication Headers
//        urlRequest.setValue(Constants.TOKEN_TYPE + UserManager.default.token, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    
}
