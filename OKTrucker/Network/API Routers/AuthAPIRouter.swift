//
//  AuthAPIRouter.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import Alamofire

enum AuthAPIRouter: URLRequestConvertible {
    
    //The endpoint name we'll call later
    case logInWithTelephone(_ telephone: String, _ password: String, _ firebaseToken: String, _ type: String)
    case signUp(
        _ firstName: String,
        _ lastName: String,
        _ username: String,
        _ email: String,
        _ password: String,
        _ confirmPassword: String,
        _ address: String,
        _ companyName: String,
        _ telephoneNumber: String,
        _ driverTypeId: Int,
        _ driverLicenseId: String,
        _ driverLicenseType: String,
        _ citizenId: String,
        _ truckTypeId: Int,
        _ licensePlate: String,
        _ truckProvince: String,
        _ truckYear: String,
        _ truckModel: String,
        _ truckBrand: String,
        _ logisteeId: Int)
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .logInWithTelephone:
            return .post
        case .signUp:
            return .post
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .logInWithTelephone:
            return "token"
        case .signUp:
            return "signup/driver"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
            
        case .logInWithTelephone(let telephone, let password, let firebaseToken, let type):
            return ["username" : telephone, "password" : password, "token" : firebaseToken, "type" : type]
            
        case .signUp(let firstName, let lastName, let username, let email, let password, let confirmPassword, let address, let companyName, let telephoneNumber, let driverTypeId, let driverLicenseId, let driverLicenseType, let citizenId, let truckTypeId, let licensePlate, let truckProvince, let truckYear, let truckModel, let truckBrand, let logisteeId):
            
            return [
                "first_name" : firstName,
                "last_name" : lastName,
                "username" : username,
                "email" : email,
                "password" : password,
                "password_confirmation" : confirmPassword,
                "current_address" : address,
                "company_name" : companyName,
                "telephone_number" : telephoneNumber,
                "driver_type_id" : driverTypeId,
                "driver_license_id" : driverLicenseId,
                "driver_license_type" : driverLicenseType,
                "citizen_id" : citizenId,
                "car_type_id" : truckTypeId,
                "license_plate" : licensePlate,
                "car_province" : truckProvince,
                "car_year" : truckYear,
                "car_model" : truckModel,
                "car_brand" : truckBrand,
                "logistee_id" : logisteeId
            ]
        }
    }
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.BASE_API_URL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}
