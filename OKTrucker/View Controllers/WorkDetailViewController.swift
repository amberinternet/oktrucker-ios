//
//  WorkDetailViewController.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class WorkDetailViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var statusLabel: WorkStatusLabel!
    @IBOutlet weak var truckTypeLabel: UILabel!
    @IBOutlet weak var workIdLabel: UILabel!
    @IBOutlet weak var sourceAddressLabel: UILabel!
    @IBOutlet weak var sourceDateLabel: UILabel!
    @IBOutlet weak var sourceContactView: ContactView!
    @IBOutlet weak var destinationAddressLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var destinationContactView: ContactView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    @IBOutlet weak var itemWeightLabel: UILabel!
    @IBOutlet weak var additionalServiceView: AdditionalServiceView!
    @IBOutlet weak var remarkView: RemarkView!
    @IBOutlet weak var customerView: CustomerView!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentMethodIcon: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var finalWageLabel: UILabel!
    @IBOutlet weak var paymentStatusView: PaymentStatusView!
    @IBOutlet weak var actionView: ActionView!
    private var viewModel: WorkDetailViewModel!
    private var dialogManager: DialogManager!
    let refreshControl = UIRefreshControl()
    var temporaryWork: Work!
    var isAcceptWork: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initRefreshControl()
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = WorkDetailViewModel(self)
        viewModel.work = temporaryWork
        dialogManager = DialogManager.default()
    }
    
    private func initRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshWork), for: .valueChanged)
        scrollView.refreshControl = refreshControl
    }
    
    @objc private func refreshWork() {
        viewModel.isRefresh = true
        disposable = viewModel.getWorkDetail()
        print("ScrollView is refresh.")
    }
    
    func initView() {
        truckTypeLabel.text = viewModel.work.truckType.name
        workIdLabel.text = viewModel.work.getReadableWorkId()
        switch viewModel.work.status {
        case WorkStatus.FIND_TRUCK.status, WorkStatus.WAITING_CONFIRM_WORK.status, WorkStatus.COMPLETE.status, WorkStatus.CANCEL.status:
            sourceAddressLabel.text = viewModel.work.source.getShortAddress()
            destinationAddressLabel.text = viewModel.work.destination.getShortAddress()
        default:
            sourceAddressLabel.text = viewModel.work.source.getFullAddress()
            destinationAddressLabel.text = viewModel.work.destination.getFullAddress()
        }
        sourceDateLabel.text = viewModel.work.source.getReceiveDateTime()
        sourceContactView.conpanyNameLabel.text = viewModel.work.source.companyName
        sourceContactView.landmarkLabel.text = viewModel.work.source.getReadableLandmark()
        sourceContactView.contactLabel.text = viewModel.work.source.getReadableContactName()
        destinationDateLabel.text = viewModel.work.destination.getDeliveryDateTime()
        destinationContactView.conpanyNameLabel.text = viewModel.work.destination.companyName
        destinationContactView.landmarkLabel.text = viewModel.work.destination.getReadableLandmark()
        destinationContactView.contactLabel.text = viewModel.work.destination.getReadableContactName()
        itemImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: viewModel.work.item.getItemImagePath()))
        itemNameLabel.text = viewModel.work.item.name
        itemQuantityLabel.text = viewModel.work.item.quantity
        itemWeightLabel.text = viewModel.work.item.weight
        customerView.label.text = viewModel.work.userFullName
        paymentMethodLabel.text = viewModel.work.getReadableReceiveMethod()
        switch viewModel.work.paymentMethod {
        case PaymentMethod.CASH_DESTINATION.status, PaymentMethod.CASH_SOURCE.status:
            paymentMethodIcon.image = UIImage(named: "Dollar Note Icon")
        default:
            paymentMethodIcon.image = UIImage(named: "Wallet Icon")
        }
        priceLabel.text = viewModel.work.getPrice()
        feeLabel.text = viewModel.work.getFee()
        finalWageLabel.text = viewModel.work.getFinalWage()
        initStatusView()
        initAdditionalServiceAndRemarkView()
    }
    
    private func initStatusView() {
        statusLabel.initView(work: viewModel.work)
        paymentStatusView.initView(work: viewModel.work)
        actionView.setAction(work: viewModel.work)
        
        switch viewModel.work.status {
        
        case WorkStatus.FIND_TRUCK.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: true)
        
        case WorkStatus.WAITING_CONFIRM_WORK.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: true)
        
        case WorkStatus.WAITING_RECEIVE_ITEM.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: false)
        
        case WorkStatus.ARRIVED_SOURCE.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: false)
        
        case WorkStatus.WAITING_SENT_ITEM.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: false)
        
        case WorkStatus.ARRIVED_DESTINATION.status:
            itemImageView.isHidden = false
            actionView.isHidden = false
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: false)
        
        case WorkStatus.COMPLETE.status:
            itemImageView.isHidden = true
            actionView.isHidden = true
            paymentStatusView.isHidden = false
            setCustomerView(isHidden: true)
        
        case WorkStatus.CANCEL.status:
            itemImageView.isHidden = true
            actionView.isHidden = true
            paymentStatusView.isHidden = true
            setCustomerView(isHidden: true)
            
        default:
            break
        }
    }
    
    private func setCustomerView(isHidden: Bool) {
        customerView.isHidden = isHidden
        sourceContactView.isHidden = isHidden
        destinationContactView.isHidden = isHidden
        
        if isHidden {
            sourceAddressLabel.isUserInteractionEnabled = false
            destinationAddressLabel.isUserInteractionEnabled = false
        }
        
        initCompanyAndLandmarkView()
    }
    
    private func initCompanyAndLandmarkView() {
        if (viewModel.work.source.companyName.isEmpty) {
            sourceContactView.conpanyNameLabel.isHidden = true
        }
        if (viewModel.work.source.landmark.isEmpty) {
            sourceContactView.landmarkLabel.isHidden = true
        }
        if (viewModel.work.destination.companyName.isEmpty) {
            destinationContactView.conpanyNameLabel.isHidden = true
        }
        if (viewModel.work.destination.landmark.isEmpty) {
            destinationContactView.landmarkLabel.isHidden = true
        }
    }
    
    private func initAdditionalServiceAndRemarkView() {
        // additional service
        if (!viewModel.work.additionalServiceList.isEmpty) {
            additionalServiceView.isHidden = false
            additionalServiceView.initView(additionalServiceList: viewModel.work.additionalServiceList)
        } else {
            additionalServiceView.isHidden = true
        }
        
        // remark view
        if (viewModel.work.remark != Constants.INITIAL_STRING) {
            remarkView.isHidden = false
            remarkView.label.text = viewModel.work.remark
        } else {
            remarkView.isHidden = true
        }
    }
    
    @IBAction func contactAdmin(_ sender: Any) {
        dialogManager.showContactAdmin()
    }
    
    @IBAction func openSourceLocation(_ sender: Any) {
        openGoogleMaps(latitude: viewModel.work.source.latitude, longitude: viewModel.work.source.longitude)
    }
    
    @IBAction func contactSource(_ sender: Any) {
        call(telephoneNumber: viewModel.work.source.contactTelephoneNumber)
    }
    
    @IBAction func openDestinationLocation(_ sender: Any) {
        openGoogleMaps(latitude: viewModel.work.destination.latitude, longitude: viewModel.work.destination.longitude)
    }
    
    @IBAction func contactDestination(_ sender: Any) {
        call(telephoneNumber: viewModel.work.destination.contactTelephoneNumber)
    }
    
    @IBAction func contactCustomer(_ sender: Any) {
        call(telephoneNumber: viewModel.work.userTelephoneNumber)
    }
    
    @IBAction func action(_ sender: Any) {
        switch viewModel.work.status {

        case WorkStatus.FIND_TRUCK.status:
            dialogManager.showConfirmAcceptWork(work: viewModel.work) {
                self.disposable = self.viewModel.acceptWork()
            }

        case WorkStatus.WAITING_CONFIRM_WORK.status:
            dialogManager.showContactAdmin()

        case WorkStatus.WAITING_RECEIVE_ITEM.status:
            let currentDate = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            let workDate = viewModel.work.source.date.fromDate()
            if currentDate >= workDate {
                dialogManager.showConfirm(title: NSLocalizedString("ยืนยันถึงต้นทาง", comment: ""), message: NSLocalizedString("คุณมาถึงต้นทางแล้วใช่หรือไม่", comment: "")) {
                    self.disposable = self.viewModel.arrivedSource()
                }
            } else {
                dialogManager.showAlert(message: NSLocalizedString("ไม่สามารถถึงต้นทางได้เนื่องจากวันนี้ไม่ใช่วันขึ้นของ" , comment: ""))
            }
            

        case WorkStatus.ARRIVED_SOURCE.status:
            if (viewModel.work.paymentMethod == PaymentMethod.CASH_SOURCE.status) {
                dialogManager.showAlertReceiveCash(paymentMethod: NSLocalizedString("คุณมีเงินสดเก็บจากต้นทาง", comment: ""), price: viewModel.work.getPrice()) {
                    self.dialogManager.showConfirmSignature(title: NSLocalizedString("ลายเซ็นต้นทาง", comment: ""), completion: { (signatureImage) in
                        let signatureData = signatureImage.jpegData(compressionQuality: 1.0)!
                        self.disposable = self.viewModel.departSource(signatureData: signatureData)
                    })
                }
            } else {
                dialogManager.showConfirmSignature(title: NSLocalizedString("ลายเซ็นต้นทาง", comment: ""), completion: { (signatureImage) in
                    let signatureData = signatureImage.jpegData(compressionQuality: 1.0)!
                    self.disposable = self.viewModel.departSource(signatureData: signatureData)
                })
            }

        case WorkStatus.WAITING_SENT_ITEM.status:
            let currentDate = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            let workDate = viewModel.work.destination.date.fromDate()
            if currentDate >= workDate {
                dialogManager.showConfirm(title: NSLocalizedString("ยืนยันถึงปลายทาง", comment: ""), message: NSLocalizedString("คุณมาถึงปลายทางแล้วใช่หรือไม่", comment: "")) {
                    self.disposable = self.viewModel.arrivedDestination()
                }
            } else {
                dialogManager.showAlert(message: NSLocalizedString("ไม่สามารถถึงปลายทางได้เนื่องจากวันนี้ไม่ใช่วันลงของ" , comment: ""))
            }

        case WorkStatus.ARRIVED_DESTINATION.status:
            if (viewModel.work.paymentMethod == PaymentMethod.CASH_DESTINATION.status) {
                dialogManager.showAlertReceiveCash(paymentMethod: NSLocalizedString("คุณมีเงินสดเก็บจากปลายทาง", comment: ""), price: viewModel.work.getPrice()) {
                    self.dialogManager.showConfirmSignature(title: NSLocalizedString("ลายเซ็นปลายทาง", comment: ""), completion: { (signatureImage) in
                        let signatureData = signatureImage.jpegData(compressionQuality: 1.0)!
                        self.disposable = self.viewModel.completeWork(signatureData: signatureData)
                    })
                }
            } else {
                dialogManager.showConfirmSignature(title: NSLocalizedString("ลายเซ็นปลายทาง", comment: ""), completion: { (signatureImage) in
                    let signatureData = signatureImage.jpegData(compressionQuality: 1.0)!
                    self.disposable = self.viewModel.completeWork(signatureData: signatureData)
                })
            }

        default:
            break
        }
    }
    
    private func openGoogleMaps(latitude: String, longitude: String) {
        if (UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)) {
            // open app
            UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
        } else {
            // open web
            UIApplication.shared.open(URL(string: "https://www.google.com/maps/search/?api=1&query=\(latitude),\(longitude)")!)
        }
    }
    
    private func call(telephoneNumber: String) {
        guard let number = URL(string: "tel://" + telephoneNumber) else {
            print("Failed to convert \"\(telephoneNumber)\" to URL.")
            return
        }
        UIApplication.shared.open(number)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is FullScreenImageViewController
        {
            let viewController = segue.destination as! FullScreenImageViewController
            viewController.imagePath = viewModel.work.item.getItemImagePath()
            viewController.title = viewModel.work.item.name
        }
        
    }
    
}
