//
//  SignupPage1ViewController.swift
//  OKTrucker
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

class SignupPage1ViewController: BaseViewController {
    
    @IBOutlet weak var firstNameTextField: DesignableTextField!
    @IBOutlet weak var lastNameTextField: DesignableTextField!
    @IBOutlet weak var emailTextField: DesignableTextField!
    @IBOutlet weak var citizenIdTextField: DesignableTextField!
    @IBOutlet weak var currentAddressTextField: MultilineTextField!
    @IBOutlet weak var driverLicenseIdTextField: DesignableTextField!
    @IBOutlet weak var driverLlicenseTypeDropDownView: DropDownView!
    @IBOutlet weak var driverTypeDropDownView: DropDownView!
    @IBOutlet weak var companyView: CompanyView!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        citizenIdTextField.set(maximumCharacters: 13)
        driverLlicenseTypeDropDownView.initDropDown(data: Arrays.license_types)
//        driverTypeDropDownView.initDropDown(data: Arrays.driver_types) { (id, item) in
//            if id == 1 { self.companyView.isHidden = true }
//            else { self.companyView.isHidden = false }
//        }
//        companyView.isHidden = true
    }
    
    @IBAction func next(_ sender: Any) {
        if validate() {
            User.shared.firstName = firstNameTextField.text!
            User.shared.lastName = lastNameTextField.text!
            User.shared.email = emailTextField.text!
            User.shared.driver.citizenId = citizenIdTextField.text!
            User.shared.address = currentAddressTextField.text!
            User.shared.driver.driverLicenseId = driverLicenseIdTextField.text!
            User.shared.driver.driverLicenseType = driverLlicenseTypeDropDownView.selectedItem!
            User.shared.driver.driverTypeId = 1 //driverTypeDropDownView.selectedId!
            User.shared.driver.companyName = "" //companyView.textField.text!
            
            let parentViewController = self.parent as! SignupViewController
            parentViewController.setPage(number: 2)
        }
    }
    
    private func validate() -> Bool {
        if firstNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกชื่อ", comment: ""))
            return false
        }
        if lastNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกนามสกุล", comment: ""))
            return false
        }
        if emailTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกอีเมล", comment: ""))
            return false
        }
        if !emailTextField.isValidEmail {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกอีเมลให้ถูกต้อง", comment: ""))
            return false
        }
        if citizenIdTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเลขที่บัตรประชาชน", comment: ""))
            return false
        }
        if currentAddressTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกอยู่ปัจจุบัน", comment: ""))
            return false
        }
        if driverLicenseIdTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเลขที่ใบขับขี่", comment: ""))
            return false
        }
        if driverLlicenseTypeDropDownView.selectedId == nil {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกประเภทใบขับขี่", comment: ""))
            return false
        }
//        if driverTypeDropDownView.selectedId == nil {
//            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกประเภทผู้ขับ", comment: ""))
//            return false
//        }
//        if driverTypeDropDownView.selectedId! > 1 && companyView.textField.isNilOrEmpty {
//            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกชื่อบริษัท", comment: ""))
//            return false
//        }
        return true
    }
    
}
