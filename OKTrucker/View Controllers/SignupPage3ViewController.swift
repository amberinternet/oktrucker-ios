//
//  SignupPage3ViewController.swift
//  OKTrucker
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class SignupPage3ViewController: BaseViewController {

    @IBOutlet weak var telephoneTextField: DesignableTextField!
    @IBOutlet weak var passwordTextField: DesignableTextField!
    @IBOutlet weak var confirmPasswordTextField: DesignableTextField!
    @IBOutlet weak var checkBox: CheckBox!
    private var viewModel: SignupPage3ViewModel!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = SignupPage3ViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        telephoneTextField.set(maximumCharacters: 10)
    }

    @IBAction func showAcceptMessage(_ sender: Any) {
        let acceptMessageDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        acceptMessageDialog.view.tintColor = UIColor(named: "Primary Color")
        
        var message = NSLocalizedString("ข้อตกลงและเงื่อนไขการใช้บริการ", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "TermsAndConditionsSegue", sender: sender)
        }))
        
        message = NSLocalizedString("นโยบายความเป็นส่วนตัว", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "PrivacyPolicySegue", sender: sender)
        }))
        
        message = NSLocalizedString("ยอมรับ", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ (UIAlertAction)in
            self.checkBox.isChecked = true
        }))
        
        message = NSLocalizedString("ยกเลิก", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .cancel, handler:{ (UIAlertAction)in
            self.checkBox.isChecked = false
        }))
        
        self.present(acceptMessageDialog, animated: true, completion: nil)
    }
    
    @IBAction func signUp(_ sender: Any) {
        if validate() {
            User.shared.telephoneNumber = telephoneTextField.text!
            User.shared.username = telephoneTextField.text!
            User.shared.password = passwordTextField.text!
            User.shared.confirmPassword = confirmPasswordTextField.text!
            disposable = viewModel.signUp()
        }
    }
    
    private func validate() -> Bool {
        if telephoneTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเบอร์โทรศัพท์", comment: ""))
            return false
        }
        if !telephoneTextField.isValidTelephone {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเบอร์โทรศัพท์ 10 หลัก", comment: ""))
            return false
        }
        if passwordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกรหัสผ่าน", comment: ""))
            return false
        }
        if confirmPasswordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณายืนยันรหัสผ่าน", comment: ""))
            return false
        }
        if confirmPasswordTextField.text != passwordTextField.text {
            dialogManager.showAlert(message: NSLocalizedString("รหัสผ่านไม่ตรงกัน\nกรุณาตรวจสอบใหม่", comment: ""))
            return false
        }
        if !checkBox.isChecked {
            dialogManager.showAlert(message: NSLocalizedString("กรุณายอมรับเงื่อนไขการใช้บริการและนโยบายความเป็นส่วนตัว", comment: ""))
            return false
        }
        return true
    }
    
}
