//
//  DepositViewController.swift
//  OKTrucker
//
//  Created by amberhello on 3/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class DepositViewController: BaseViewController {
    
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        user = User.shared
    }
    
    private func initView() {
        creditLabel.text = user.getCredit()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? PaymentMethodViewController{
            let selectedCell = sender as! DepositAmountViewCell
            let indexPath = tableView.indexPath(for: selectedCell)!
            let price = Arrays.deposit_amount_list[indexPath.item]
            viewController.price = price
        }
        
    }

}

extension DepositViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Arrays.deposit_amount_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell { let cell = tableView.dequeueReusableCell(withIdentifier: "DepositAmountCell", for: indexPath) as! DepositAmountViewCell
        cell.label.text = Arrays.deposit_amount_list[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
