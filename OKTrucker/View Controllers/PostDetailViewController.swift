//
//  PostDetailViewController.swift
//  OKTrucker
//
//  Created by amberhello on 9/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class PostDetailViewController: BaseViewController {
    
    @IBOutlet weak var postIdLabel: UILabel!
    @IBOutlet weak var receivedDateLabel: UILabel!
    @IBOutlet weak var receivedTimeLabel: UILabel!
    @IBOutlet weak var sourceProvinceLabel: UILabel!
    @IBOutlet weak var deliveredDateLabel: UILabel!
    @IBOutlet weak var deliveredTimeLabel: UILabel!
    @IBOutlet weak var destinationProvinceLabel: UILabel!
    @IBOutlet weak var passedProvincesLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    private var viewModel: PostDetailViewModel!
    private var dialogManager: DialogManager!
    var post: Post!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = PostDetailViewModel(self)
        dialogManager = DialogManager.default()
    }
        
    private func initView() {
        postIdLabel.text = post.formattedPostId
        receivedDateLabel.text = post.receivedDate.fromDate().toDateWithFullDayAppFormat()
        receivedTimeLabel.text = post.getReadable(time: post.receivedTime)
        sourceProvinceLabel.text = post.sourceProvince
        deliveredDateLabel.text = post.deliveredDate.fromDate().toDateWithFullDayAppFormat()
        deliveredTimeLabel.text = post.getReadable(time: post.deliveredTime)
        destinationProvinceLabel.text = post.destinationProvince
        if !post.passedProvinces.isEmpty { passedProvincesLabel.text = post.passedProvinces.joined(separator:"\n") }
        else { passedProvincesLabel.text = "-" }
        if !post.remark.isEmpty { remarkLabel.text = post.remark }
        else { remarkLabel.text = "-" }
    }
    
    @IBAction func deletePost(_ sender: Any) {
        dialogManager.showConfirm(title: NSLocalizedString("ยืนยันลบโพสต์", comment: ""), message: NSLocalizedString("ยืนยันลบโพสต์นี้หรือไม่", comment: "")) {
            self.disposable = self.viewModel.deletePost(postId: self.post.id)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? CreatePostViewController {
            viewController.temporaryPost = post
        }
        
    }

}
