//
//  SecondViewController.swift
//  OKTrucker
//
//  Created by amberhello on 18/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import AlamofireImage

class SettingViewController: BaseViewController {
    
    @IBOutlet weak var buildVersionLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var driveIdLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var truckLabel: UILabel!
    @IBOutlet weak var telephoneLabel: UILabel!
    @IBOutlet weak var bankAccountLabel: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    private var viewModel: SettingViewModel!
    private var dialogManager: DialogManager!
    private var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = SettingViewModel(self)
        user = User.shared
        dialogManager = DialogManager.default()
    }
    
    private func initView() {
        buildVersionLabel.text = version()
        profileImageView.af_setImage(withURL: URL(string: user.getFullProfileImageURI())!)
        driveIdLabel.text = user.driver.readableDriverId
        fullNameLabel.text = user.getFullName()
        truckLabel.text = "\(user.driver.truck.truckType.name) / \(user.driver.truck.licensePlate)"
        telephoneLabel.text = user.telephoneNumber
        bankAccountLabel.text = "ธนาคาร\(user.driver.bankAccount.bank) / \(user.driver.bankAccount.accountNumber)"
        joinedLabel.text = user.driver.createdAt.fromDateTime().toDateAppFormat()
    }

    @IBAction func logout(_ sender: Any) {
        dialogManager.showConfirm(title: NSLocalizedString("ยืนยันออกจากระบบ", comment: ""), message: NSLocalizedString("คุณต้องการออกจากระบบใช่หรือไม่", comment: "")) {
            self.viewModel.logOut()
        }
    }
    
    private func version() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "แอพเวอร์ชั่น: \(version) build \(build)"
    }
    
}

