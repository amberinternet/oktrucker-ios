//
//  MainTabBarController.swift
//  OKTrucker
//
//  Created by amberhello on 8/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import CoreLocation

class MainTabBarController: UITabBarController {
    
    private var viewModel: MainTabBarModel!
    private var dialogManager: DialogManager!
    private let locationManager = CLLocationManager()
    private let timeInterval = TimeInterval(5 * 60)

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateCurrentLocation()
    }
    
    private func initVariable() {
        viewModel = MainTabBarModel()
        dialogManager = DialogManager.default()
        locationManager.delegate = self
    }
    
    private func initView() {
        selectedIndex = 2
    }

}

extension MainTabBarController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations[0].coordinate
        viewModel.updateLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
        
        print("Fetch & update location: \(currentLocation.latitude), \(currentLocation.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
            
        case .authorizedAlways, .authorizedWhenInUse:
            Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(updateCurrentLocation), userInfo: nil, repeats: true)
            
        case .notDetermined:
            manager.requestAlwaysAuthorization()
            
        default:
            if !CLLocationManager.locationServicesEnabled() {
                let title = NSLocalizedString("บริการตำแหน่งที่ตั้งปิดอยู่", comment: "")
                let message = NSLocalizedString("กรุณาเปิดบริการตำแหน่งที่ตั้งในการตั้งค่า", comment: "")
                dialogManager.showAlert(title: title, message: message) {
                    UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
                }
            } else {
                let title = NSLocalizedString("การเข้าถึงตำแหน่งที่ตั้งถูกปฏิเสธ", comment: "")
                let message = NSLocalizedString("กรุณาเปิดใช้งานการเข้าถึงตำแหน่งที่ตั้งในการตั้งค่า", comment: "")
                let settingsUrl = UIApplication.openSettingsURLString
                dialogManager.showRequestAccessDialog(title: title, message: message, settingsUrl: settingsUrl)
            }
            
        }
        
    }
    
    @objc func updateCurrentLocation() {
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}
