//
//  CreditCardMethodViewController.swift
//  OKTrucker
//
//  Created by amberhello on 4/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import OmiseSDK

class CreditCardMethodViewController: BaseViewController {
    
    @IBOutlet weak var creditCardNumberTextField: CardNumberTextField!
    @IBOutlet weak var expiryMonthTextField: CardExpiryMonthTextField!
    @IBOutlet weak var expiryYearTextField: CardExpiryYearTextField!
    @IBOutlet weak var cvvTextField: CardCVVTextField!
    @IBOutlet weak var creditCardNameTextField: UITextField!
    private var viewModel: CreditCardMethodViewModel!
    private var dialogManager: DialogManager!
    var price: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    private func initVariable() {
        viewModel = CreditCardMethodViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    @IBAction func confirm(_ sender: Any) {
        if validate() {
            dialogManager.showLoading(message: "กำลังตรวจสอบข้อมูล...")
            let cardName = creditCardNameTextField.text!
            let cardNumber = creditCardNumberTextField.pan
            let expMonth = Int(expiryMonthTextField.text!)!
            let ecpYear = Int(expiryYearTextField.text!)!
            let cvv = cvvTextField.text!
            
            let client = Client(publicKey: "pkey_test_5ezt65lmm4vt9n9q1dy")
            let tokenRequest = Request<Token>(name: cardName,
                                              pan: cardNumber,
                                              expirationMonth: expMonth,
                                              expirationYear: ecpYear,
                                              securityCode: cvv)
            client.send(tokenRequest) { (result) in
                switch result {
                case .success(let token):
                    self.disposable = self.viewModel.requestDepositWithOmise(price: self.price, token: token.id)
                case .failure(let error):
                    self.dialogManager.hideLoading()
                    self.dialogManager.showError(error: error.localizedDescription)
                }
            }
        }
    }
    
    func showAuthorizingPaymentForm(authorizeURI: String) {
        print(authorizeURI)
        let authorizeURL = URL(string: authorizeURI)!
        let expectedReturnURL = URLComponents(string: Constants.BASE_URL)!
        let handlerController = AuthorizingPaymentViewController.makeAuthorizingPaymentViewControllerNavigationWithAuthorizedURL(authorizeURL, expectedReturnURLPatterns: [expectedReturnURL], delegate: self)
        present(handlerController, animated: true, completion: nil)
    }
    
    private func validate() -> Bool {
        if creditCardNumberTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกหมายเลขบัตร", comment: ""))
            return false
        }
        if !creditCardNumberTextField.isValid {
            dialogManager.showAlert(message: NSLocalizedString("หมายเลขบัตรไม่ถูกต้อง", comment: ""))
            return false
        }
        if expiryMonthTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกวันหมดอายุบัตร", comment: ""))
            return false
        }
        if !expiryMonthTextField.validate() {
            dialogManager.showAlert(message: NSLocalizedString("วันหมดอายุบัตรไม่ถูกต้อง", comment: ""))
            return false
        }
        if expiryYearTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกวันหมดอายุบัตร", comment: ""))
            return false
        }
        if !expiryYearTextField.validate() {
            dialogManager.showAlert(message: NSLocalizedString("วันหมดอายุบัตรไม่ถูกต้อง", comment: ""))
            return false
        }
        if cvvTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกรหัส CVV", comment: ""))
            return false
        }
        if !cvvTextField.isValid {
            dialogManager.showAlert(message: NSLocalizedString("รหัส CVV ไม่ถูกต้อง", comment: ""))
            return false
        }
        if creditCardNameTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกชื่อบนบัตร", comment: ""))
            return false
        }
        return true
    }

}

extension CreditCardMethodViewController: AuthorizingPaymentViewControllerDelegate {
    func authorizingPaymentViewController(_ viewController: AuthorizingPaymentViewController, didCompleteAuthorizingPaymentWithRedirectedURL redirectedURL: URL) {
        print(redirectedURL)
        dismiss(animated: true, completion: {
            self.disposable = self.viewModel.completePaymentOmise()
        })
    }
    
    func authorizingPaymentViewControllerDidCancel(_ viewController: AuthorizingPaymentViewController) {
        dismiss(animated: true, completion: nil)
    }
}
