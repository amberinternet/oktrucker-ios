//
//  TransactionRequestDetailViewController.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class TransactionRequestDetailViewController: BaseViewController {
    
    @IBOutlet weak var statusLabel: TransactionRequestStatusLabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var paymentMethodView: PaymentMethodView!
    @IBOutlet weak var transferDateView: TransactionRequestDetailSubview!
    @IBOutlet weak var remarkView: TransactionRequestDetailSubview!
    @IBOutlet weak var slipImageView: UIImageView!
    var deposit: Deposit? = nil
    var withdraw: Withdraw? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        if let request = deposit {
            
            statusLabel.initView(deposit: request)
            dateLabel.text = request.createdAt.fromDateTime().toDateTimeAppFormat()
            nameLabel.text = request.readableId
            transactionTypeLabel.text = NSLocalizedString("เติมเงิน", comment: "")
            creditLabel.text = request.getReadableCredit()
            if request.type == DepositType.TRANSFER.value {
                paymentMethodView.icon.image = UIImage(named: "Invoice Icon")
            } else {
                paymentMethodView.icon.image = UIImage(named: "Card Icon")
            }
            paymentMethodView.label.text = request.getReadablePaymentMethod()
            transferDateView.isHidden = true
            
            if (request.remark.isNilOrEmpty) {
                remarkView.isHidden = true
            } else {
                remarkView.label.text = request.remark
            }
            
            if (request.type == DepositType.OMISE.value) {
                slipImageView.isHidden = true
            } else {
                slipImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: request.getSlipImagePath()))
            }
            
        } else if let request = withdraw {
            
            statusLabel.initView(withdraw: request)
            dateLabel.text = request.createdAt.fromDateTime().toDateTimeAppFormat()
            nameLabel.text = request.getReadableStatus()
            transactionTypeLabel.text = NSLocalizedString("ถอนเงิน", comment: "")
            creditLabel.text = request.getReadableCredit()
            paymentMethodView.isHidden = true
            
            if (request.payTime.isNilOrEmpty) {
                transferDateView.isHidden = true
            } else {
                transferDateView.label.text = request.getReadableTransferDate()
            }
            
            if (request.remark.isNilOrEmpty) {
                remarkView.isHidden = true
            } else {
                remarkView.label.text = request.remark
            }
            
            if (request.status == TransactionRequestStatus.APPROVED.status) {
                slipImageView.af_setImage(withURLRequest: ImageAPIRouter.getItemImage(imagePath: request.getSlipImagePath()))
            } else {
                slipImageView.isHidden = true
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination is FullScreenImageViewController
        {
            let viewController = segue.destination as! FullScreenImageViewController
            if let request = deposit {
                viewController.imagePath = request.getSlipImagePath()
            } else if let request = withdraw {
                viewController.imagePath = request.getSlipImagePath()
            }
            viewController.title = NSLocalizedString("หลักฐานการโอนเงิน", comment: "")
        }
        
    }

}
