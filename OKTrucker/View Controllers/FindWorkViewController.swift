//
//  FindWorkViewController.swift
//  OKTrucker
//
//  Created by amberhello on 18/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class FindWorkViewController: UICollectionViewController {
    
    private var viewModel: FindWorkViewModel!
    let refreshControl = UIRefreshControl()
    private var query: String!
    private var disposable: Disposable? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshWorkList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }

    private func initVariable() {
        viewModel = FindWorkViewModel(self)
        query = Constants.INITIAL_STRING
    }
    
    private func initView() {
        // Fit cell to screen width
        let collectionViewLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = UIScreen.main.bounds.width - (collectionViewLayout.minimumInteritemSpacing * 2)
        collectionViewLayout.itemSize = CGSize(width: cellWidth, height: collectionViewLayout.itemSize.height)
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWorkList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshWorkList() {
        viewModel.isRefresh = true
        loadWorkList(page: 1)
        print("CollectionView is refresh.")
    }
    
    private func loadWorkList(page: Int) {
        let provinceNames = query.components(separatedBy: ",")
        
        for provinceName in provinceNames {
            disposable = viewModel.getWorkList(page: page, sort: WorkQuery.SORT_DESC.value, query: provinceName)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? WorkDetailViewController {
            let selectedCell = sender as! WorkViewCell
            let indexPath = collectionView.indexPath(for: selectedCell)!
            let work = viewModel.workList[indexPath.item]
            
            viewController.temporaryWork = work
        }
        
        if let viewController = segue.destination as? FilterWorkViewController{
            viewController.province =  query
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? WorkDetailViewController {
            if sourceViewController.isAcceptWork {
                self.tabBarController!.selectedIndex = 2 // my work tab
            }
        }
        
        if let sourceViewController = unwindsSegue.source as? FilterWorkViewController {
            query = sourceViewController.province!
        }
        
    }
    
    // collectionView dataSource & delegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.workList.isEmpty {
            return 1
        }
        
        return viewModel.workList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.workList.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkCell", for: indexPath) as! WorkViewCell
        cell.initView(work: viewModel.workList[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.workPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.workList.count - 1) {
            loadWorkList(page: viewModel.workPagination.currentPage + 1)
        }
    }

}
