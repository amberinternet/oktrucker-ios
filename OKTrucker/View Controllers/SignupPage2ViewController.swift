//
//  SignupPage2ViewController.swift
//  OKTrucker
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

class SignupPage2ViewController: BaseViewController {
    
    @IBOutlet weak var truckTypeDropDownView: DropDownView!
    @IBOutlet weak var licensePlateTextField: DesignableTextField!
    @IBOutlet weak var truckYearTextField: DesignableTextField!
    @IBOutlet weak var truckBrandTextField: DesignableTextField!
    @IBOutlet weak var truckModelTextField: DesignableTextField!
    @IBOutlet weak var truckProvinceDropDownView: DropDownView!
    private var dialogManager: DialogManager!
    private var truckTypeList: [TruckType] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        dialogManager = DialogManager.default()
        
        for i in 3...9 {
            truckTypeList.append(TruckType(id: i, name: Arrays.truck_type_names[i-1], imageURI: "Truck Type \(i) Image"))
        }
        truckTypeList = truckTypeList.reversed()
    }
    
    private func initView() {
        truckTypeDropDownView.initDropDown(data: Arrays.truck_type_names[2...].reversed())
        truckTypeDropDownView.dropDown.cellNib = UINib(nibName: "TruckTypeDropDownCell", bundle: nil)
        truckTypeDropDownView.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? TruckTypeDropDownCell else { return }
            cell.truckImageView.image = UIImage(named: self.truckTypeList[index].imageURI)
        }
        
        truckProvinceDropDownView.initDropDown(data: Arrays.provinces)
    }
    
    @IBAction func next(_ sender: Any) {
        if validate() {
            User.shared.driver.truck.truckType = truckTypeList[truckTypeDropDownView.selectedIndex!]
            User.shared.driver.truck.licensePlate = licensePlateTextField.text!
            User.shared.driver.truck.truckYear = truckYearTextField.text!
            User.shared.driver.truck.truckBrand = truckBrandTextField.text!
            User.shared.driver.truck.truckModel = truckModelTextField.text!
            User.shared.driver.truck.truckProvince = truckProvinceDropDownView.selectedItem!
            
            let parentViewController = self.parent as! SignupViewController
            parentViewController.setPage(number: 3)
        }
    }
    
    private func validate() -> Bool {
        if truckTypeDropDownView.selectedId == nil {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกประเภทรถบรรทุก", comment: ""))
            return false
        }
        if licensePlateTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเลขทะเบียนรถ", comment: ""))
            return false
        }
        if truckYearTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกปีรถ", comment: ""))
            return false
        }
        if truckBrandTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกยี่ห้อรถ", comment: ""))
            return false
        }
        if truckModelTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกรุ่นรถ", comment: ""))
            return false
        }
        if truckProvinceDropDownView.selectedId == nil {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกจังหวัดรถ", comment: ""))
            return false
        }
        return true
    }

}
