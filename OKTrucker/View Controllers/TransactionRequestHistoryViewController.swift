//
//  TransactionRequestHistoryViewController.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class TransactionRequestHistoryViewController: UITableViewController {
    
    @IBOutlet weak var requestTypeDropDownView: DropDownView!
    @IBOutlet weak var monthDropDownView: DropDownView!
    @IBOutlet weak var yearDropDownView: DropDownView!
    private var viewModel: TransactionRequestHistoryViewModel!
    private var disposable: Disposable? = nil
    private var selectedRequestType: Int { return requestTypeDropDownView.selectedId! }

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = TransactionRequestHistoryViewModel(self)
        refreshControl = UIRefreshControl()
    }
    
    private func initView() {
        // Refresh Control
        refreshControl!.addTarget(self, action: #selector(refreshRequestHistory), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        requestTypeDropDownView.initDropDown(data: Arrays.transaction_request_types) { (_,_) in
            self.refreshRequestHistory()
        }
        requestTypeDropDownView.selectRow(at: 0)
        
        monthDropDownView.initDropDown(data: DateFormatter().monthSymbols) { (_,_) in
            self.refreshRequestHistory()
        }
        monthDropDownView.selectRow(at: Date().getMonth() - 1)
        
        let startYear = User.shared.driver.createdAt.fromDateTime().getYear()
        let currentYear = Date().getYear()
        let buddhistYearList = (startYear...currentYear).map { String($0 + 543) }
        yearDropDownView.initDropDown(data: buddhistYearList) { (_,_) in
            self.refreshRequestHistory()
        }
        yearDropDownView.selectRow(at: Date().getYear() - startYear)
        
        refreshRequestHistory()
    }
    
    @objc private func refreshRequestHistory() {
        viewModel.isRefresh = true
        loadRequestHistory(page: 1)
        print("TableView is refresh.")
        print("\(requestTypeDropDownView.selectedItem!) \(getMonthYearFilter())")
    }
    
    private func loadRequestHistory(page: Int) {
        if selectedRequestType == 1 {
            disposable = viewModel.getDepositList(page: page, date: getMonthYearFilter())
        } else {
            disposable = viewModel.getWithdrawList(page: page, date: getMonthYearFilter())
        }
    }
    
    func getMonthYearFilter() -> String {
        let year: Int = Int(yearDropDownView.selectedItem!)! - 543
        let month: String = String(format: "%02d", monthDropDownView.selectedId!)
        return "\(year)-\(month)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? TransactionRequestDetailViewController {
            let selectedCell = sender as! TransactionRequestViewCell
            let indexPath = tableView.indexPath(for: selectedCell)!
            let request = viewModel.requestHistory[indexPath.item]
            
            if selectedRequestType == 1 {
                viewController.deposit = (request as! Deposit)
            } else {
                viewController.withdraw = (request as! Withdraw)
            }
        }
        
    }
    
    // tableView dataSource & delegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.requestHistory.isEmpty {
            return 1
        }
        
        return viewModel.requestHistory.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.requestHistory.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let request = viewModel.requestHistory[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionRequestCell", for: indexPath) as! TransactionRequestViewCell
        if selectedRequestType == 1 {
            cell.initView(deposit: request as! Deposit)
        } else {
            cell.initView(withdraw: request as! Withdraw)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == (viewModel.requestHistory.count - 1) {
            var nextPage: Int
            if selectedRequestType == 1 {
                nextPage = viewModel.depositPagination.currentPage + 1
            } else {
                nextPage = viewModel.withdrawPagination.currentPage + 1
            }
            loadRequestHistory(page: nextPage)
        }
    }

}
