//
//  FilterTransactionViewController.swift
//  OKTrucker
//
//  Created by amberhello on 1/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class FilterTransactionViewController: BaseViewController {
    
    @IBOutlet weak var monthDropDownView: DropDownView!
    @IBOutlet weak var yearDropDownView: DropDownView!
    private var yearList: [Int]!
    var month: Int!
    var year: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        monthDropDownView.initDropDown(data: DateFormatter().monthSymbols)
        monthDropDownView.selectRow(at: month - 1)
        
        let startYear = User.shared.driver.createdAt.fromDateTime().getYear()
        let currentYear = Date().getYear()
        yearList = (startYear...currentYear).map { $0 }
        let buddhistYearList = (startYear...currentYear).map { String($0 + 543) }
        yearDropDownView.initDropDown(data: buddhistYearList)
        yearDropDownView.selectRow(at: year - startYear)
    }
    
    @IBAction func filter(_ sender: Any) {
        if let id = monthDropDownView.selectedId {
            month = id
        }
        if let index = yearDropDownView.selectedIndex {
            year = yearList[index]
        }

        performSegue(withIdentifier: "FilterTransactionUnwindSegue", sender: nil)
    }

}
