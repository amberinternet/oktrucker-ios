//
//  PaymentMethodViewController.swift
//  OKTrucker
//
//  Created by amberhello on 3/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

import UIKit

class PaymentMethodViewController: BaseViewController {
    
    @IBOutlet weak var priceLabel: UILabel!
    var price: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        priceLabel.text = price
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? TransferMethodViewController {
            viewController.price = Int(price.replacingOccurrences(of: "฿", with: ""))
        }
        
        if let viewController = segue.destination as? CreditCardMethodViewController {
            viewController.price = Int(price.replacingOccurrences(of: "฿", with: ""))
        }
        
    }
    
}

