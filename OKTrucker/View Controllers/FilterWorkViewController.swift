//
//  FilterWorkViewController.swift
//  OKTrucker
//
//  Created by amberhello on 26/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

class FilterWorkViewController: BaseViewController {
    
    @IBOutlet weak var provinceTextField: AutocompleteTextField!
    var province: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        provinceTextField.initDropDown(data: Arrays.filter_work_provinces) { (id, item) in
            self.provinceTextField.text = item.components(separatedBy: ",")[0]
        }
        provinceTextField.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            cell.optionLabel.text = item.components(separatedBy: ",")[0]
        }
        
        if !province.isEmpty {
            provinceTextField.text = province?.components(separatedBy: ",")[0]
        }
    }
    
    @IBAction func filter(_ sender: Any) {
        if let index = provinceTextField.selectedIndex {
            if provinceTextField.selectedItem == Arrays.filter_work_provinces[0] {
                province = Constants.INITIAL_STRING
            } else {
                province = Arrays.filter_work_provinces[index]
            }
        }
        performSegue(withIdentifier: "FilterWorkUnwindSegue", sender: nil)
    }
    
}
