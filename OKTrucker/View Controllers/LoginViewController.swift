//
//  LogInViewController.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var telephoneTextField: DesignableTextField!
    @IBOutlet weak var passwordTextField: DesignableTextField!
    @IBOutlet weak var logInButton: RoundButton!
    @IBOutlet weak var loadingIndicator: IndicatorImageView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var acceptMessageLabel: UILabel!
    @IBOutlet var registerTapGestureRecognizer: UITapGestureRecognizer!
    private var viewModel: LoginViewModel!
    private var dialogManager: DialogManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initVariable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func initView() {
        telephoneTextField.set(maximumCharacters: 10)
        
        let message = NSLocalizedString("โดยการคลิ๊กที่ปุ่มลงชื่อเข้าใช้หรือปุ่มลงทะเบียนผู้ใช้หมายความว่าคุณยอมรับใน ข้อตกลงและเงื่อนไขการใช้บริการและนโยบายความเป็นส่วนตัวของแอพพลิเคชั่นโอเคทรัค", comment: "")
        let subMessage1 = NSLocalizedString("ข้อตกลงและเงื่อนไขการใช้บริการ", comment: "")
        let subMessage2 = NSLocalizedString("นโยบายความเป็นส่วนตัว", comment: "")
        
        let attributedString = NSMutableAttributedString.init(string: message)
        var range = (message as NSString).range(of: subMessage1)
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        range = (message as NSString).range(of: subMessage2)
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue , range: range)
        
        acceptMessageLabel.attributedText = attributedString
    }
    
    private func initVariable() {
        viewModel = LoginViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    @IBAction func logIn(_ sender: Any) {
        if (validate()) {
            logInWithTelephone()
        }
    }
    
    @IBAction func showAcceptMessage(_ sender: Any) {
        let acceptMessageDialog = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        acceptMessageDialog.view.tintColor = UIColor(named: "Primary Color")
        
        var message = NSLocalizedString("ข้อตกลงและเงื่อนไขการใช้บริการ", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "TermsAndConditionsSegue", sender: sender)
        }))
        
        message = NSLocalizedString("นโยบายความเป็นส่วนตัว", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .default , handler:{ (UIAlertAction)in
            self.performSegue(withIdentifier: "PrivacyPolicySegue", sender: sender)
        }))
        
        message = NSLocalizedString("ยกเลิก", comment: "")
        acceptMessageDialog.addAction(UIAlertAction(title: message, style: .cancel, handler: nil))
        
        self.present(acceptMessageDialog, animated: true, completion: nil)
    }
    
    private func validate() -> Bool {
        if telephoneTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเบอร์โทรศัพท์", comment: ""))
            return false
        }
        if telephoneTextField.text!.isInvalidTelephone() {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกเบอร์โทรศัพท์ 10 หลัก", comment: ""))
            return false
        }
        if passwordTextField.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกรหัสผ่าน", comment: ""))
            return false
        }
        return true
    }
    
    func enabledView(isEnabled: Bool) {
        telephoneTextField.isUserInteractionEnabled = isEnabled
        passwordTextField.isUserInteractionEnabled = isEnabled
        logInButton.isUserInteractionEnabled = isEnabled
        signUpButton.isUserInteractionEnabled = isEnabled
        acceptMessageLabel.isUserInteractionEnabled = isEnabled
    }
    
    private func logInWithTelephone() {
        // TODO:  getFirebaseToken
        disposable = viewModel.logInWithTelephone(telephone: telephoneTextField.text!, password: passwordTextField.text!, firebaseToken: "")
    }
    
    func goToMainStoryBoard() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateInitialViewController()!
        self.present(nextViewController, animated:true, completion:nil)
    }

}
