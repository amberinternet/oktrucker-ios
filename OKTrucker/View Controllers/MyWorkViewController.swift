//
//  MyWorkViewController.swift
//  OKTrucker
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyWorkViewController: ButtonBarPagerTabStripViewController {
    
    private var query: String!

    override func viewDidLoad() {
        initView()
        super.viewDidLoad()
    }
    
    private func initView() {
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = UIColor(named: "Primary Color")!
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        
        changeCurrentIndexProgressive = { (unselectedCell: ButtonBarViewCell?, selectedCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            unselectedCell?.label.textColor = UIColor(named: "Placeholder Color")
            unselectedCell?.label.font = UIFont.systemFont(ofSize: 16.0)
            selectedCell?.label.textColor = UIColor(named: "Text Color")
            selectedCell?.label.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let myInProgressWorkTab = storyboard!.instantiateViewController(withIdentifier: "MyInProgressWorkView") as! MyInProgressWorkViewController
        let myCompleteWorkTab = storyboard!.instantiateViewController(withIdentifier: "MyCompleteWorkView") as! MyCompleteWorkViewController
        return [myInProgressWorkTab, myCompleteWorkTab]
    }

}
