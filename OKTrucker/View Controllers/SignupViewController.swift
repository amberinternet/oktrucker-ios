//
//  SignupViewController.swift
//  OKTrucker
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    @IBOutlet weak var checkedPage1Icon: UIImageView!
    @IBOutlet weak var checkedPage2Icon: UIImageView!
    @IBOutlet weak var titlePage1Label: UILabel!
    @IBOutlet weak var titlePage2Label: UILabel!
    @IBOutlet weak var titlePage3Label: UILabel!
    @IBOutlet weak var signupPage1View: UIView!
    @IBOutlet weak var signupPage2View: UIView!
    @IBOutlet weak var signupPage3View: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBackButton()
        setPage(number: 1)
    }

    func setPage(number: Int) {
        switch number {
        case 1:
            checkedPage1Icon.isHidden = true
            checkedPage2Icon.isHidden = true
            
            titlePage1Label.isHighlighted = true
            titlePage2Label.isHighlighted = false
            titlePage3Label.isHighlighted = false
            
            signupPage1View.isHidden = false
            signupPage2View.isHidden = true
            signupPage3View.isHidden = true
        case 2:
            checkedPage1Icon.isHidden = false
            checkedPage2Icon.isHidden = true
            
            titlePage1Label.isHighlighted = false
            titlePage2Label.isHighlighted = true
            titlePage3Label.isHighlighted = false
            
            signupPage1View.isHidden = true
            signupPage2View.isHidden = false
            signupPage3View.isHidden = true
        case 3:
            checkedPage1Icon.isHidden = false
            checkedPage2Icon.isHidden = false
            
            titlePage1Label.isHighlighted = false
            titlePage2Label.isHighlighted = false
            titlePage3Label.isHighlighted = true
            
            signupPage1View.isHidden = true
            signupPage2View.isHidden = true
            signupPage3View.isHidden = false
        default:
            break
        }
    }
    
    func setNavigationBarBackButton() {
        let backButton = UIButton(type: .system)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -8.0, bottom: 0, right: 0)
        
        if #available(iOS 13.0, *) {
            backButton.setImage((navigationController?.navigationBar.subviews[1].subviews[0].subviews[0].subviews[0] as! UIImageView).image, for: .normal)
        } else {
            backButton.setImage((navigationController?.navigationBar.subviews[2].subviews[0].subviews[0].subviews[0] as! UIImageView).image, for: .normal)
        }
        
        backButton.setTitle(" " + NSLocalizedString("ย้อนกลับ", comment: ""), for: .normal)
        backButton.titleLabel!.font =  UIFont.systemFont(ofSize: 17.0)
        backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        let backBarButton = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = backBarButton
    }
    
    @objc private func back() {
        if !signupPage1View.isHidden {
            self.navigationController?.popViewController(animated: true)
        }
        if !signupPage2View.isHidden {
            setPage(number: 1)
        }
        if !signupPage3View.isHidden {
            setPage(number: 2)
        }
    }
}
