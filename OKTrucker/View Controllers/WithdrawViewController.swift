//
//  WithdrawViewController.swift
//  OKTrucker
//
//  Created by amberhello on 2/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class WithdrawViewController: BaseViewController {
    
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var withdrawalAmountTextField: DesignableTextField!
    private var viewModel: WithdrawViewModel!
    private var dialogManager: DialogManager!
    private var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    private func initVariable() {
        viewModel = WithdrawViewModel(self)
        dialogManager = DialogManager.default()
        user = User.shared
    }
    
    private func initView() {
        creditLabel.text = user.getCredit()
    }

    @IBAction func confirmWithdraw(_ sender: Any) {
        if validate() {
            disposable = viewModel.requestWithdraw(price: Int(withdrawalAmountTextField.text!)!)
        }
    }
    
    private func validate() -> Bool {
        if let credit = Double(withdrawalAmountTextField.text!) {
            
            if credit < 100 {
                dialogManager.showAlert(message: NSLocalizedString("คุณสามารถถอนเงินได้ขั้นต่ำ 100 บาท", comment: ""))
                return false
            }
            if credit > user.creditBalance {
                dialogManager.showAlert(message: NSLocalizedString("คุณไม่สามารถถอนเงินเกินเครดิตในกระเป๋าตังได้", comment: ""))
                return false
            }
            return true
            
        } else {
            dialogManager.showAlert(message: NSLocalizedString("กรุณากรอกจำนวนเงินที่ต้องการถอน", comment: ""))
            return false
        }
    }
    
}
