//
//  ExplanationViewController.swift
//  OKTrucker
//
//  Created by amberhello on 6/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class ExplanationViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}
