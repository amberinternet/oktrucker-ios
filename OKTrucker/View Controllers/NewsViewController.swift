//
//  NewsViewController.swift
//  OKTrucker
//
//  Created by amberhello on 17/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class NewsViewController: UICollectionViewController {
    
    private var viewModel: NewsViewModel!
    let refreshControl = UIRefreshControl()
    private var disposable: Disposable? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshNewsList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = NewsViewModel(self)
    }
    
    private func initView() {
        // Fit cell to screen width
        let collectionViewLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = UIScreen.main.bounds.width - (collectionViewLayout.minimumInteritemSpacing * 2)
        collectionViewLayout.itemSize = CGSize(width: cellWidth, height: collectionViewLayout.itemSize.height)
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshNewsList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshNewsList() {
        viewModel.isRefresh = true
        disposable = viewModel.getNewsList()
        print("CollectionView is refresh.")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is NewsDetailViewController
        {
            let selectedCell = sender as! NewsViewCell
            let indexPath = collectionView.indexPath(for: selectedCell)!
            let news = viewModel.newsList[indexPath.item]
            
            let viewController = segue.destination as! NewsDetailViewController
            viewController.temporaryNews = news
        }
    }
    
    // collectionView dataSource & delegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.newsList.isEmpty {
            return 1
        }
        
        return viewModel.newsList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.newsList.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = 45
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! NewsViewCell
        cell.initView(news: viewModel.newsList[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.newsPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.newsList.count - 1) {
            disposable = viewModel.getNewsList(page: viewModel.newsPagination.currentPage + 1)
        }
    }

}
