//
//  PostViewController.swift
//  OKTrucker
//
//  Created by amberhello on 3/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import RxSwift

class PostViewController: UICollectionViewController {
    
    private var viewModel: PostViewModel!
    let refreshControl = UIRefreshControl()
    private var disposable: Disposable? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initView()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshWorkList()
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        disposable?.dispose()
    }
    
    private func initVariable() {
        viewModel = PostViewModel(self)
    }
    
    private func initView() {
        // Fit cell to screen width
        let collectionViewLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = UIScreen.main.bounds.width - (collectionViewLayout.minimumInteritemSpacing * 2)
        collectionViewLayout.itemSize = CGSize(width: cellWidth, height: collectionViewLayout.itemSize.height)
        
        // Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWorkList), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc private func refreshWorkList() {
        viewModel.isRefresh = true
        loadPostList(page: 1)
        print("CollectionView is refresh.")
    }
    
    private func loadPostList(page: Int) {
        disposable = viewModel.getPostList(page: page)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let viewController = segue.destination as? PostDetailViewController {
            let indexPath = sender as! IndexPath
            let post = viewModel.postList[indexPath.item]

            viewController.post = post
        }
        
        if let viewController = segue.destination as? WorkDetailViewController {
            let indexPath = sender as! IndexPath
            let work = viewModel.postList[indexPath.item].work
            
            viewController.temporaryWork = work
        }
        
    }
    
    @IBAction func prepare(for unwindsSegue: UIStoryboardSegue) {
        
        if let sourceViewController = unwindsSegue.source as? WorkDetailViewController {
            if sourceViewController.isAcceptWork {
                self.tabBarController!.selectedIndex = 2 // my work tab
            }
        }
        
    }

    // collectionView dataSource & delegate
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.postList.isEmpty {
            return 1
        }
        
        return viewModel.postList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if viewModel.postList.isEmpty {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCell", for: indexPath)
            cell.frame.size.height = collectionView.bounds.height - (tabBarController?.tabBar.frame.height ?? 0)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath) as! PostViewCell
        cell.initView(post: viewModel.postList[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !viewModel.postList.isEmpty {
            if viewModel.postList[indexPath.item].work == nil {
                performSegue(withIdentifier: "PostDetailSegue", sender: indexPath)
            } else {
                performSegue(withIdentifier: "WorkDetailSegue", sender: indexPath)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.postPagination == nil {
            return
        }
        
        if indexPath.item == (viewModel.postList.count - 1) {
            loadPostList(page: viewModel.postPagination.currentPage + 1)
        }
    }

}
