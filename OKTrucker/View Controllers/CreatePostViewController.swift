//
//  CreatePostViewController.swift
//  OKTrucker
//
//  Created by amberhello on 9/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit
import DropDown

class CreatePostViewController: BaseViewController {
    
    @IBOutlet weak var receivedDateDropDownView: DropDownView!
    @IBOutlet weak var receivedTimeDropDownView: DropDownView!
    @IBOutlet weak var sourceProvinceTextField: AutocompleteTextField!
    @IBOutlet weak var deliveredDateDropDownView: DropDownView!
    @IBOutlet weak var deliveredTimeDropDownView: DropDownView!
    @IBOutlet weak var destinationProvinceTextField: AutocompleteTextField!
    @IBOutlet weak var passedProvincesStackView: UIStackView!
    @IBOutlet weak var remarkTextField: DesignableTextField!
    private var viewModel: CreatePostViewModel!
    private var dialogManager: DialogManager!
    var temporaryPost: Post?
    
    private var post: Post!

    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    private func initVariable() {
        viewModel = CreatePostViewModel(self)
        dialogManager = DialogManager.default()
    }
    
    func initView() {
        var dateList = [String]()
        var startDate = Date()
        dateList.append(startDate.toDateWithFullDayAppFormat())
        for _ in 1...7 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
            startDate = tomorrow
            dateList.append(startDate.toDateWithFullDayAppFormat())
        }
        receivedDateDropDownView.initDropDown(data: dateList) { _,_ in
            dateList = [String]()
            startDate = self.receivedDateDropDownView.selectedItem?.fromDateWithFullDayAppFormat() ?? Date()
            dateList.append(startDate.toDateWithFullDayAppFormat())
            for _ in 1...2 {
                let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
                startDate = tomorrow
                dateList.append(startDate.toDateWithFullDayAppFormat())
            }
            self.deliveredDateDropDownView.initDropDown(data: dateList)
            self.deliveredDateDropDownView.text = NSLocalizedString("วันที่ถึงปลายทาง", comment: "")
        }
        receivedTimeDropDownView.initDropDown(data: Arrays.hour_time_list)
        initProvinceTextField(sourceProvinceTextField)
        
        dateList = [String]()
        startDate = Date()
        dateList.append(startDate.toDateWithFullDayAppFormat())
        for _ in 1...2 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: startDate)!
            startDate = tomorrow
            dateList.append(startDate.toDateWithFullDayAppFormat())
        }
        deliveredDateDropDownView.initDropDown(data: dateList)
        deliveredTimeDropDownView.initDropDown(data: Arrays.duration_time_list)
        initProvinceTextField(destinationProvinceTextField)
        
        remarkTextField.setTextCountdown(maximumCharacters: 30)
        
        if let post = temporaryPost {
            sourceProvinceTextField.setSelected(item: post.sourceProvince)
            destinationProvinceTextField.setSelected(item: post.destinationProvince)
            for province in post.passedProvinces {
                addPassedProvince(province)
            }
            remarkTextField.text = post.remark
        }
    }
    
    func initProvinceTextField(_ provinceTextField: AutocompleteTextField) {
        provinceTextField.initDropDown(data: Arrays.provinces)
        provinceTextField.text = ""
    }
    
    @IBAction func addPassedProvince(_ sender: Any) {
        if passedProvincesStackView.subviews.count < 10 {
            let passedProvinceView = PassedProvinceView(frame: CGRect(x: 0, y: 0, width: passedProvincesStackView.bounds.width, height: 40))
            passedProvinceView.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
            passedProvinceView.initView()
            passedProvincesStackView.addArrangedSubview(passedProvinceView)
            
            if sender is String {
                passedProvinceView.provinceTextField.setSelected(item: sender as! String)
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func createPost(_ sender: Any) {
        if validate() {
            if temporaryPost == nil { post = Post() }
            else { post = temporaryPost }
            post.sourceProvince = sourceProvinceTextField.selectedItem!
            post.receivedDate = receivedDateDropDownView.selectedItem!.fromDateWithFullDayAppFormat().toDateServerFormat()
            post.receivedTime = receivedTimeDropDownView.selectedItem!
            post.destinationProvince = destinationProvinceTextField.selectedItem!
            post.deliveredDate = deliveredDateDropDownView.selectedItem!.fromDateWithFullDayAppFormat().toDateServerFormat()
            post.deliveredTime = deliveredTimeDropDownView.selectedItem!
            post.passedProvinces.removeAll()
            for passedProvinceView in passedProvincesStackView.subviews as! [PassedProvinceView] {
                if let province = passedProvinceView.provinceTextField.selectedItem {
                    post.passedProvinces.append(province)
                }
            }
            post.remark = remarkTextField.text!
            disposable = viewModel.createPost(post!)
        }
    }
    
    private func validate() -> Bool {
        if receivedDateDropDownView.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกวันที่ออกเดินทาง", comment: ""))
            return false
        }
        if receivedTimeDropDownView.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกเวลาออกเดินทาง", comment: ""))
            return false
        }
        if sourceProvinceTextField.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกจังหวัดต้นทาง", comment: ""))
            return false
        }
        if deliveredDateDropDownView.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกวันที่ถึงปลายทาง", comment: ""))
            return false
        }
        if deliveredTimeDropDownView.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกเวลาถึงปลายทาง", comment: ""))
            return false
        }
        if destinationProvinceTextField.selectedItem.isNilOrEmpty {
            dialogManager.showAlert(message: NSLocalizedString("กรุณาเลือกจังหวัดปลายทาง", comment: ""))
            return false
        }
        return true
    }

}
