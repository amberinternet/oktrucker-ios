//
//  SettingViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

class SettingViewModel {
    
    private var viewController: SettingViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    private var userManager: UserManager!
    
    init(_ viewControllor: SettingViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
        userManager = UserManager.default
    }
    
    private func getFirebaseToken(callback: (_ token: String) -> Void) {
        // TODO: getFirebaseToken
    }
    
    func logOut() {
        viewController.disposable = RxNetwork<SuccessResponse>().request(userService.destroyToken(firebaseToken: Constants.INITIAL_STRING), onSuccess: { (response) in
            User.shared.clear()
            self.userManager.destroyAllData()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationController")
            self.viewController.present(nextViewController, animated:true, completion:nil)
        }, onFailure: { (_) in }) // TODO: FirebaseToken
    }
    
}
