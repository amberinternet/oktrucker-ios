//
//  MainTabBarModel.swift
//  OKTrucker
//
//  Created by amberhello on 9/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class MainTabBarModel {
    
    private var userService: UserService!
    
    init() {
        userService = UserService.default
    }
    
    func updateLocation(latitude: Double, longitude: Double) {
//        let observable: Observable<SuccessResponse> = userService.refreshToken().flatMap({ value -> Observable<SuccessResponse> in
//            UserManager.default.storeAccessToken(value.accessToken)
//            return self.userService.updateLocation(latitude: latitude, longitude: longitude)
//        })
//        _ = RxNetwork<SuccessResponse>().request(observable)
        
        _ = RxNetwork<SuccessResponse>().request(userService.updateLocation(latitude: latitude, longitude: longitude))
    }
    
}
