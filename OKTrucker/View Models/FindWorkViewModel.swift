//
//  FindWorkViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 10/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class FindWorkViewModel {
    
    private var viewController: FindWorkViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var workList: [Work]! = []
    
    var workPagination: WorkPagination! {
        didSet {
            if workPagination != oldValue && workPagination != nil {
                viewController.collectionView.reloadData()
                print("Loaded page: \(workPagination.currentPage!)")
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                workPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }
    
    init(_ viewControllor: FindWorkViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getWorkList(page: Int = 1, sort: String, query: String? = "") -> Disposable? {
        return RxNetwork<WorkListResponse>().request(userService.getWork(page: page, search: query!, sort: sort), onSuccess: { (response) in
            if (self.isRefresh) {
                self.workList.removeAll()
                self.isRefresh = false
            }
            
            self.workList.append(contentsOf: response.workPagination.workList)
            self.workPagination = response.workPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
}
