//
//  SignupPage3ViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 19/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class SignupPage3ViewModel {
    
    private var viewController: SignupPage3ViewController!
    private var dialogManager: DialogManager!
    private var authService: AuthService!
    private var user: User!
    
    init(_ viewControllor: SignupPage3ViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        authService = AuthService.default
    }
    
    func signUp() -> Disposable? {
        user = User.shared
        return RxNetwork<SuccessResponse>().request(authService.signUp(
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            email: user.email,
            password: user.password,
            confirmPassword: user.confirmPassword,
            address: user.address,
            companyName: user.driver.companyName,
            telephoneNumber: user.telephoneNumber,
            driverTypeId: user.driver.driverTypeId,
            driverLicenseId: user.driver.driverLicenseId,
            driverLicenseType: user.driver.driverLicenseType,
            citizenId: user.driver.citizenId,
            truckTypeId: user.driver.truck.truckType.id,
            licensePlate: user.driver.truck.licensePlate,
            truckProvince: user.driver.truck.truckProvince,
            truckYear: user.driver.truck.truckYear,
            truckModel: user.driver.truck.truckModel,
            truckBrand: user.driver.truck.truckBrand,
            logisteeId: 1
        ), onSuccess: { (_) in
            self.dialogManager.showSuccess(title: NSLocalizedString("สมัครสมาชิกสำเร็จ", comment: ""), message: NSLocalizedString("สมัครสมาชิกสำเร็จ กรุณารอแอดมินเพื่อยืนยัน\nขอบคุณค่ะ", comment: ""), completion: {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNavigationController")
                self.viewController.present(nextViewController, animated:true, completion:nil)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังสมัครสมาชิก...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
