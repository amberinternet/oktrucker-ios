//
//  WalletViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 29/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class WalletViewModel {
    
    private var viewController: WalletViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var transactionList: [Transaction]! = []
    
    var transactionPagination: TransactionPagination! {
        didSet {
            if transactionPagination != oldValue && transactionPagination != nil {
                viewController.tableView.reloadData()
                print("Loaded page: \(transactionPagination.currentPage!)")
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                transactionPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }
    
    init(_ viewControllor: WalletViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getTransactionList(page: Int = 1, date: String) -> Disposable? {
        return RxNetwork<TransactionsListResponse>().request(userService.getTransaction(page: page, date: date), onSuccess: { (response) in
            if (self.isRefresh) {
                self.transactionList.removeAll()
                self.isRefresh = false
            }
            
            self.transactionList.append(contentsOf: response.transactionPagination.transactionList)
            self.transactionPagination = response.transactionPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
    func getProfile() -> Disposable? {
        return RxNetwork<UserResponse>().request(userService.getProfile(), onSuccess: { (response) in
            User.shared.set(user: response.user)
            self.viewController.creditLabel.text = User.shared.getCredit()
        }, onFailure: { (_) in })
    }
    
}
