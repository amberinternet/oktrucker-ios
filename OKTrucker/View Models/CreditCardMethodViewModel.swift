//
//  CreditCardMethodViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 4/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreditCardMethodViewModel {
    
    private var viewController: CreditCardMethodViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    var depositOmise: DepositOmise! {
        didSet {
            viewController.showAuthorizingPaymentForm(authorizeURI: depositOmise.authorizeURI)
        }
    }
    
    init(_ viewControllor: CreditCardMethodViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func requestDepositWithOmise(price: Int, token: String) -> Disposable? {
        return RxNetwork<DepositOmiseResponse>().request(userService.depositWithOmise(type: DepositType.OMISE.value, price: price, omiseToken: token, fromApp: 1), onSuccess: { (response) in
            self.depositOmise = response.depositOmise
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งคำขอ...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func completePaymentOmise() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.completePaymentOmise(depositId: depositOmise.deposit.id), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("ชำระเงินสำเร็จ", comment: ""), completion: {
                self.viewController.performSegue(withIdentifier: "CreditCardMethodUnwindSegue", sender: nil)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังตรวจสอบการชำระเงิน...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
