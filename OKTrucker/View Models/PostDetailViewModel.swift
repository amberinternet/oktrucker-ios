//
//  PostDetailViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 9/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class PostDetailViewModel {
    
    private var viewController: PostDetailViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: PostDetailViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func deletePost(postId: Int) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.deletePost(postId: postId), onSuccess: { _ in
            self.dialogManager.showSuccess(message: NSLocalizedString("ลบโพสต์สำเร็จ", comment: ""), completion: {
                self.viewController.navigationController?.popViewController(animated: true)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}

