//
//  WithdrawViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 2/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class WithdrawViewModel {
    
    private var viewController: WithdrawViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: WithdrawViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func requestWithdraw(price: Int) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.withdraw(price: price), onSuccess: { (_) in
            self.dialogManager.showSuccess(message: NSLocalizedString("ส่งคำขอสำเร็จ", comment: ""), completion: {
                self.viewController.navigationController?.popViewController(animated: true)
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งคำขอ...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
