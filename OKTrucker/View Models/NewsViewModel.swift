//
//  NewsViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 17/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class NewsViewModel {
    
    private var viewController: NewsViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var newsList: [News]! = []
    
    var newsPagination: NewsPagination! {
        didSet {
            if newsPagination != oldValue && newsPagination != nil {
                viewController.collectionView.reloadData()
                print("Loaded page: \(newsPagination.currentPage!)")
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl.beginRefreshing()
                newsPagination = nil
            } else {
                viewController.refreshControl.endRefreshing()
            }
        }
    }
    
    init(_ viewControllor: NewsViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getNewsList(page: Int = 1) -> Disposable? {
        return RxNetwork<NewsListResponse>().request(userService.getNews(page: page), onSuccess: { (response) in
            if (self.isRefresh) {
                self.newsList.removeAll()
                self.isRefresh = false
            }
            
            self.newsList.append(contentsOf: response.newsPagination.newsList)
            self.newsPagination = response.newsPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
}
