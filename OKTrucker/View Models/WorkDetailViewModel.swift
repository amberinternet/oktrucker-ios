//
//  WorkDetailViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 12/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift
import Alamofire

class WorkDetailViewModel {
    
    private var viewController: WorkDetailViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var work: Work!
    
    var status: Int! {
        didSet {
            if status != oldValue {
                viewController.initView()
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true { viewController.refreshControl.beginRefreshing() }
            else { viewController.refreshControl.endRefreshing() }
            
            if (work != viewController.temporaryWork) {
                viewController.temporaryWork = work
                viewController.initView()
                print("The work has changed.")
            }
        }
    }
    
    init(_ viewControllor: WorkDetailViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getWorkDetail() -> Disposable? {
        return RxNetwork<WorkResponse>().request(userService.getWorkDetail(workId: work.id), onSuccess: { (response) in
            print("Loaded work: \(self.work.getReadableWorkId())")
            if (self.isRefresh) {
                self.work = response.work
                self.isRefresh = false
            }
        }, onFailure: { (error) in
            if error == "forbidden" {
                self.dialogManager.showAlert(message: NSLocalizedString("งานถูกรับไปแล้ว", comment: "")) {
                    self.viewController.navigationController?.popViewController(animated: true)
                }
            } else {
                self.dialogManager.showError(error: error)
            }
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
    func acceptWork() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.acceptWork(workId: work.id), onSuccess: { (_) in
            self.work.status = WorkStatus.WAITING_CONFIRM_WORK.status
            self.dialogManager.showSuccess(message: NSLocalizedString("รับงานสำเร็จ", comment: "")) {
                self.status = WorkStatus.WAITING_CONFIRM_WORK.status
                self.viewController.isAcceptWork = true
                self.viewController.performSegue(withIdentifier: "FindWorkUnwindSegue", sender: nil)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func arrivedSource() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.arrivedSource(workId: work.id), onSuccess: { (_) in
            self.work.status = WorkStatus.ARRIVED_SOURCE.status
            self.dialogManager.showSuccess(message: NSLocalizedString("ถึงต้นทางแล้ว", comment: "")) {
                self.status = WorkStatus.ARRIVED_SOURCE.status
                self.viewController.navigationController?.popViewController(animated: true)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func departSource(signatureData: Data) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.departSource(workId: work.id, signatureData: signatureData), onSuccess: { (_) in
            self.work.status = WorkStatus.WAITING_SENT_ITEM.status
            self.dialogManager.showSuccess(message: NSLocalizedString("ขึ้นของสำเร็จ", comment: "")) {
                self.status = WorkStatus.WAITING_SENT_ITEM.status
                self.viewController.navigationController?.popViewController(animated: true)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func arrivedDestination() -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.arrivedDestination(workId: work.id), onSuccess: { (_) in
            self.work.status = WorkStatus.ARRIVED_DESTINATION.status
            self.dialogManager.showSuccess(message: NSLocalizedString("ถึงปลายทางแล้ว", comment: "")) {
                self.status = WorkStatus.ARRIVED_DESTINATION.status
                self.viewController.navigationController?.popViewController(animated: true)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
    func completeWork(signatureData: Data) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.completeWork(workId: work.id, signatureData: signatureData), onSuccess: { (_) in
            self.work.status = WorkStatus.COMPLETE.status
            self.dialogManager.showSuccess(message: NSLocalizedString("ทำงานเสร็จแล้ว", comment: ""), completion: {
                self.status = WorkStatus.COMPLETE.status
            })
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }

}
