//
//  CreatePostViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 9/12/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class CreatePostViewModel {
    
    private var viewController: CreatePostViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    
    init(_ viewControllor: CreatePostViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func createPost(_ post: Post) -> Disposable? {
        return RxNetwork<SuccessResponse>().request(userService.createPost(
            sourceProvince: post.sourceProvince,
            receivedDate: post.receivedDate,
            receivedTime: post.receivedTime,
            destinationProvince: post.destinationProvince,
            deliveredDate: post.deliveredDate,
            deliveredTime: post.deliveredTime,
            passedProvinces: post.passedProvinces.joined(separator:","),
            remark: post.remark
        ), onSuccess: { (response) in
            self.dialogManager.showSuccess(message: NSLocalizedString("สร้างโพสต์สำเร็จ", comment: "")) {
                self.viewController.navigationController?.popViewController(animated: true)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.dialogManager.showLoading(message: NSLocalizedString("กำลังส่งข้อมูล...", comment: ""))
        }, onLoaded: {
            self.dialogManager.hideLoading()
        })
    }
    
}
