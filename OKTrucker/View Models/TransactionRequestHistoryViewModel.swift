//
//  TransactionRequestHistoryViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 6/7/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class TransactionRequestHistoryViewModel {
    
    private var viewController: TransactionRequestHistoryViewController!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    var requestHistory: [Any]! = []
    
    var depositPagination: DepositPagination! {
        didSet {
            if depositPagination != oldValue && depositPagination != nil {
                viewController.tableView.reloadData()
                print("Loaded deposit page: \(depositPagination.currentPage!)")
            }
        }
    }
    
    var withdrawPagination: WithdrawPagination! {
        didSet {
            if withdrawPagination != oldValue && withdrawPagination != nil {
                viewController.tableView.reloadData()
                print("Loaded withdraw page: \(withdrawPagination.currentPage!)")
            }
        }
    }
    
    var isRefresh: Bool! = false {
        didSet {
            if isRefresh == true {
                viewController.refreshControl!.beginRefreshing()
                depositPagination = nil
                withdrawPagination = nil
            } else {
                viewController.refreshControl!.endRefreshing()
            }
        }
    }
    
    init(_ viewControllor: TransactionRequestHistoryViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
    }
    
    func getDepositList(page: Int = 1, date: String) -> Disposable? {
        return RxNetwork<DepositListResponse>().request(userService.getDeposit(page: page, date: date), onSuccess: { (response) in
            if (self.isRefresh) {
                self.requestHistory.removeAll()
                self.isRefresh = false
            }
            
            self.requestHistory.append(contentsOf: response.depositPagination.depositList)
            self.depositPagination = response.depositPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
    func getWithdrawList(page: Int = 1, date: String) -> Disposable? {
        return RxNetwork<WithdrawListResponse>().request(userService.getWithdraw(page: page, date: date), onSuccess: { (response) in
            if (self.isRefresh) {
                self.requestHistory.removeAll()
                self.isRefresh = false
            }
            
            self.requestHistory.append(contentsOf: response.withdrawPagination.withdrawList)
            self.withdrawPagination = response.withdrawPagination
            
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
            if (self.isRefresh) {
                self.isRefresh = false
            }
        })
    }
    
}

