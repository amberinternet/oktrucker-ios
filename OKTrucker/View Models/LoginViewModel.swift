//
//  LoginViewModel.swift
//  OKTrucker
//
//  Created by amberhello on 8/6/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import RxSwift

class LoginViewModel {
    
    private var viewController: LoginViewController!
    private var userManager: UserManager!
    private var dialogManager: DialogManager!
    private var userService: UserService!
    private var authService: AuthService!
    
    var logInSuccess: Bool! = false {
        didSet {
            if logInSuccess != oldValue {
                viewController.goToMainStoryBoard()
            }
        }
    }
    
    init(_ viewControllor: LoginViewController) {
        viewController = viewControllor
        dialogManager = DialogManager.default()
        userService = UserService.default
        authService = AuthService.default
        userManager = UserManager.default
    }
    
    private func getFirebaseToken(callback: (_ token: String) -> Void) {
        // TODO: getFirebaseToken
    }
    
    func logInWithTelephone(telephone: String, password: String, firebaseToken: String) -> Disposable? {
        let observable: Observable<UserResponse> = authService.logInWithTelephone(telephone: telephone, password: password, firebaseToken: firebaseToken, type: UserType.DRIVER.type).flatMap ({ value -> Observable<UserResponse> in
            UserManager.default.storeAccessToken(value.accessToken)
            return self.userService.getProfile()
        })
        
        return RxNetwork<UserResponse>().request(observable, onSuccess: { (response) in
            User.shared.set(user: response.user)
            if (User.shared.isVerify == 1) {
                self.logInSuccess = true
                self.viewController.enabledView(isEnabled: true)
            } else {
                self.dialogManager.showAlert(message: NSLocalizedString("คุณสามารถเข้าใช้งานแอปพลิเคชันได้หลังแอดมินยืนยันข้อมูล", comment: ""))
                self.viewController.enabledView(isEnabled: true)
            }
        }, onFailure: { (error) in
            self.dialogManager.showError(error: error)
        }, onLoading: {
            self.viewController.loadingIndicator.startAnimating()
            self.viewController.enabledView(isEnabled: false)
        }, onLoaded: {
            self.viewController.loadingIndicator.stopAnimating()
            self.viewController.enabledView(isEnabled: true)
        })
    }
    
}
