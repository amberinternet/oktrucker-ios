//
//  Extension.swift
//  OKTrucker
//
//  Created by amberhello on 25/4/2562 BE.
//  Copyright © 2562 Amber Internet. All rights reserved.
//

import UIKit

extension Double {
    
    func roundDecimalFormat() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value: self.rounded()))!
    }
    
}

extension String {
    
    func fromDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: self)!
    }
    
    func fromDateTime() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: self)!
    }
    
    func fromDateWithFullDayAppFormat() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d MMM yyyy"
        dateFormatter.locale = Locale(identifier: "th")
        return dateFormatter.date(from: self)!
    }
    
    func isInvalidTelephone() -> Bool {
        return self.isEmpty || self.count != 10
    }
    
}

extension Date {
    
    func toDateServerFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
    func toDateTimeServerFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
    func toDateWithFullDayAppFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d MMM yyyy"
        dateFormatter.locale = Locale(identifier: "th")
        return dateFormatter.string(from: self)
    }
    
    func toDateAppFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = Locale(identifier: "th")
        return dateFormatter.string(from: self)
    }
    
    func toDateTimeAppFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        dateFormatter.locale = Locale(identifier: "th")
        return dateFormatter.string(from: self)
    }
    
    func getDay() -> Int {
        let day = Calendar(identifier: .gregorian).component(.day, from: self)
        return day
    }
    
    func getMonth() -> Int {
        let month = Calendar(identifier: .gregorian).component(.month, from: self)
        return month
    }
    
    func getYear() -> Int {
        let year = Calendar(identifier: .gregorian).component(.year, from: self)
        return year
    }
    
    func getHour() -> Int {
        let hour = Calendar(identifier: .gregorian).component(.hour, from: self)
        return hour
    }
    
    func getMinute() -> Int {
        let minute = Calendar(identifier: .gregorian).component(.minute, from: self)
        return minute
    }
    
}

extension Array where Element == String {
    
    func getError() -> String {
        var error = Constants.INITIAL_STRING
        for message in self {
            switch message {
            case "The username has already been taken.": error += "เบอร์โทรศัพท์นี้มีผู้ใช้งานแล้ว\n"
            case "The email has already been taken.": error += "อีเมล์นี้มีผู้ใช้งานแล้ว\n"
            case "The email must be a valid email address.": error += "อีเมล์ไม่ถูกต้อง\n"
            case "The password confirmation does not match.": error += "รหัสผ่านไม่ตรงกัน\n"
            case "The selected driver_type_id is invalid.": error += "ประเภทผู้ขับไม่ถูกต้อง\n"
            case "The selected driver_license_type is invalid.": error += "\(message)\n"
            case "The selected car_type_id is invalid.": error += "\(message)\n"
            case "The selected topup_type is invalid.": error += "\(message)\n"
            case "The selected price is invalid.": error += "\(message)\n"
            case "The selected :attribute is invalid.": error += "\(message)\n"
            default: error += "\(message)\n"
            }
        }
        return error
    }
    
}


extension UITextField {
    
    var isNilOrEmpty: Bool { return text?.isEmpty ?? true }
    
    var isValidEmail: Bool {
        let emailFormat = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+"
        
        return NSPredicate(format:"SELF MATCHES %@", emailFormat).evaluate(with: text)
    }
    
    var isValidTelephone: Bool {
        return text!.count == 10
    }
    
}

extension UIImage {
    
    func tintColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    func resize(to size: Int) -> UIImage {
        let targetSize = CGSize(width: size, height: size)
        let oldSize = self.size
        
        let widthRatio  = targetSize.width  / oldSize.width
        let heightRatio = targetSize.height / oldSize.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: oldSize.width * heightRatio, height: oldSize.height * heightRatio)
        } else {
            newSize = CGSize(width: oldSize.width * widthRatio,  height: oldSize.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension UIStackView {
    
    convenience init(axis:NSLayoutConstraint.Axis, spacing:CGFloat) {
        self.init()
        self.axis = axis
        self.spacing = spacing
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func anchorStackView(toView view:UIView, anchorX:NSLayoutXAxisAnchor, equalAnchorX:NSLayoutXAxisAnchor, anchorY:NSLayoutYAxisAnchor, equalAnchorY:NSLayoutYAxisAnchor) {
        view.addSubview(self)
        anchorX.constraint(equalTo: equalAnchorX).isActive = true
        anchorY.constraint(equalTo: equalAnchorY).isActive = true
        
    }
    
}
